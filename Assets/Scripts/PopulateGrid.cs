using Mono.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Data;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Globalization;

public class PopulateGrid : MonoBehaviour
{
    public GameObject prefab;
    public Player player;
    public AddTask task;
    public GameObject content;
    List<GameObject> events = new List<GameObject>();
    string conn;
    IDbConnection dbcon;
    IDbCommand dbcmd;
    IDataReader reader;
    void SetDb()
    {
        conn = "URI=file:" + Application.dataPath + "/db.db";
        dbcon = (IDbConnection)new SqliteConnection(conn);
        dbcon.Open();
        dbcmd = dbcon.CreateCommand();
    }

    void ClearContent()
    {
        foreach (var ev in events)
            Destroy(ev);
        events.Clear();
    }

    public void Fail(int id, Toggle[] toggles)
    {
        SetDb();
        int hp_minus = 0, type = 0;
        DateTime curr_date = DateTime.UtcNow, date = DateTime.UtcNow;
        string sql = $"SELECT * FROM todo WHERE id='{id}'";
        int maxOccurences = 0;
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        while (reader.Read())
        {
            hp_minus = reader.GetInt16(8);
            type = reader.GetInt16(2);
            date = DateTime.ParseExact(reader.GetString(5), "d.M.yyyy", null);
            maxOccurences = reader.GetInt16(10);
        }
        reader.Dispose();
        if (type == 6 || type == 5)
        {
            sql = $"DELETE FROM todo WHERE id='{id}'";
            dbcmd.CommandText = sql;
            reader = dbcmd.ExecuteReader();
            reader.Dispose();
        }
        else
        {
            Calendar cal = CultureInfo.InvariantCulture.Calendar;
            if (type == 2)//daily
            {
                while (date < curr_date)
                    date = cal.AddDays(date, 1);
            }
            else if (type == 3)//weekly
            {
                while (date < curr_date)
                    date = cal.AddWeeks(date, 1);
            }
            else if (type == 4)//monthly
            {
                while (date < curr_date)
                    date = cal.AddMonths(date, 1);
            }
            sql = $"UPDATE todo SET date='{date.ToString("d.M.yyyy")}' remainingOccurences={maxOccurences} WHERE id='{id}'";
            dbcmd.CommandText = sql;
            reader = dbcmd.ExecuteReader();
            reader.Dispose();
        }

        dbcmd.Dispose();
        dbcon.Close();
        player.AddRewards(0, 0, hp_minus);
        toggles[type].isOn = true;
        task.ShowTasks();
    }

    public void showTasks(Toggle[] toggles)
    {
        ClearContent();
        int id_task = 0;
        for (int i = 0; i < 6; i++)
        {
            if (toggles[i].isOn == true && i == 0)
            {
                int numberOfTasks = 0;
                SetDb();
                string sql = $"SELECT COUNT(*) FROM todo WHERE type={i+1} AND player_id={player.pl.id}";
                dbcmd.CommandText = sql;
                reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    numberOfTasks = reader.GetInt16(0);
                }
                reader.Dispose();

                GameObject newObj;
                DateTime curr_date = DateTime.UtcNow, date = DateTime.UtcNow;
                sql = $"SELECT * FROM todo WHERE type={i+1} AND player_id={player.pl.id}";
                dbcmd.CommandText = sql;
                reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    newObj = (GameObject)Instantiate(prefab, transform);
                    events.Add(newObj);
                    TMP_Text[] text_field = newObj.GetComponentsInChildren<TMP_Text>();
                    Button[] butoane = newObj.GetComponentsInChildren<Button>();
                    butoane[0].gameObject.SetActive(false);

                    text_field[0].text = reader.GetString(3);//nume
                    text_field[1].text = reader.GetString(4);//desc
                    //text_field[2].text = reader.GetString(12);//date
                    int gold = reader.GetInt16(7);
                    int exp = reader.GetInt16(6);
                    if (player.pl.clas == 1)
                        exp += 5;
                    if (player.pl.clas == 2)
                        exp -= 5;
                    //text_field[2].text = reader.GetString(5);//date
                    text_field[3].text = "Gold: " + gold + "\nExp: " + exp;//rewards
                    text_field[4].text = reader.GetInt16(0).ToString() + "";//id
                                                                            //data data este depasita atunci Fail(id)
                    int aux = reader.GetInt32(12); //aesthetic
                    if (aux == 1)
                        text_field[5].text = "Physical";
                    if (aux == 2)
                        text_field[5].text = "Mental";
                    if (aux == 3)
                        text_field[5].text = "Emotional";
                    curr_date = DateTime.UtcNow;
                    //date = DateTime.ParseExact(text_field[2].text, "d.M.yyyy", null);
                    Debug.Log("Data Curenta: " + curr_date);
                    Debug.Log("Data Task: " + date);
                    id_task = reader.GetInt16(0);
                }
                reader.Dispose();
                dbcmd.Dispose();
                dbcon.Close();
            }
            else if(toggles[i].isOn == true && i != 0 && i != 5)
            {
                int numberOfTasks = 0;
                SetDb();
                string sql = $"SELECT COUNT(*) FROM todo WHERE type={i + 1} AND player_id={player.pl.id}";
                dbcmd.CommandText = sql;
                reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    numberOfTasks = reader.GetInt16(0);
                }
                reader.Dispose();

                GameObject newObj;
                DateTime curr_date = DateTime.UtcNow, date = DateTime.UtcNow;
                sql = $"SELECT * FROM todo WHERE type={i + 1} AND player_id={player.pl.id}";
                dbcmd.CommandText = sql;
                reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    newObj = (GameObject)Instantiate(prefab, transform);
                    events.Add(newObj);
                    TMP_Text[] text_field = newObj.GetComponentsInChildren<TMP_Text>();
                    Button[] butoane = newObj.GetComponentsInChildren<Button>();
                    butoane[3].gameObject.SetActive(false);
                    butoane[4].gameObject.SetActive(false);

                    text_field[0].text = reader.GetString(3);//nume
                    text_field[1].text = reader.GetString(4);//desc
                    text_field[2].text = reader.GetString(5);//date
                    int gold = reader.GetInt16(7);
                    int exp = reader.GetInt16(6);
                    if (player.pl.clas == 1)
                        exp += 5;
                    if (player.pl.clas == 2)
                        exp -= 5;
                    text_field[3].text = "Gold: " + gold + "\nExp: " + exp;//rewards
                    text_field[4].text = reader.GetInt16(0).ToString() + "";//id
                                                                 //data data este depasita atunci Fail(id)

                    int aux = reader.GetInt32(12); //aesthetic
                    if (aux == 1)
                        text_field[5].text = "Physical";
                    if (aux == 2)
                        text_field[5].text = "Mental";
                    if (aux == 3)
                        text_field[5].text = "Emotional";
                    text_field[6].text = "Remaining: " + reader.GetInt16(11).ToString(); //occurences
                    curr_date = DateTime.UtcNow;
                    date = DateTime.ParseExact(text_field[2].text, "d.M.yyyy", null);
                    Debug.Log("Data Curenta: " + curr_date);
                    Debug.Log("Data Task: " + date);
                    id_task = reader.GetInt16(0);
                }
                if (curr_date > date)
                  Fail(id_task, toggles);
                reader.Dispose();
                dbcmd.Dispose();
                dbcon.Close();
            }
            else if (toggles[i].isOn == true && i == 5)
            {
                int numberOfTasks = 0;
                SetDb();
                string sql = $"SELECT COUNT(*) FROM todo WHERE type={i + 1} AND player_id={player.pl.id}";
                dbcmd.CommandText = sql;
                reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    numberOfTasks = reader.GetInt16(0);
                }
                reader.Dispose();

                GameObject newObj;
                DateTime curr_date = DateTime.UtcNow, date = DateTime.UtcNow;
                sql = $"SELECT * FROM todo WHERE type={i + 1} AND player_id={player.pl.id}";
                dbcmd.CommandText = sql;
                reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    newObj = (GameObject)Instantiate(prefab, transform);
                    events.Add(newObj);
                    TMP_Text[] text_field = newObj.GetComponentsInChildren<TMP_Text>();
                    Button[] butoane = newObj.GetComponentsInChildren<Button>();
                    butoane[1].gameObject.SetActive(false);
                    butoane[2].gameObject.SetActive(false);
                    butoane[3].gameObject.SetActive(false);
                    butoane[4].gameObject.SetActive(false);

                    text_field[0].text = reader.GetString(3);//nume
                    text_field[1].text = reader.GetString(4);//desc
                    text_field[2].text = reader.GetString(5);//date
                    int gold = reader.GetInt16(7);
                    int exp = reader.GetInt16(6);
                    if (player.pl.clas == 1)
                        exp += 5;
                    if (player.pl.clas == 2)
                        exp -= 5;
                    text_field[3].text = "Gold: " + gold + "\nExp: " + exp;//rewards
                    text_field[4].text = reader.GetInt16(0).ToString() + "";//id
                                                                 //data data este depasita atunci Fail(id)
                    curr_date = DateTime.UtcNow;
                    date = DateTime.ParseExact(text_field[2].text, "d.M.yyyy", null);
                    Debug.Log("Data Curenta: " + curr_date);
                    Debug.Log("Data Task: " + date);
                    id_task = reader.GetInt16(0);
                }
                if (curr_date > date)
                    Fail(id_task, toggles);
                reader.Dispose();
                dbcmd.Dispose();
                dbcon.Close();
            }
        }
    }

    public void showAll(Toggle[] toggles)
    {
        ClearContent();
        int id_task = 0;
        for (int i = 0; i < 6; i++)
        {
            if (i == 0)
            {
                int numberOfTasks = 0;
                SetDb();
                string sql = $"SELECT COUNT(*) FROM todo WHERE type={i + 1} AND player_id={player.pl.id}";
                dbcmd.CommandText = sql;
                reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    numberOfTasks = reader.GetInt16(0);
                }
                reader.Dispose();

                GameObject newObj;
                DateTime curr_date = DateTime.UtcNow, date = DateTime.UtcNow;
                sql = $"SELECT * FROM todo WHERE type={i + 1} AND player_id={player.pl.id}";
                dbcmd.CommandText = sql;
                reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    newObj = (GameObject)Instantiate(prefab, transform);
                    events.Add(newObj);
                    TMP_Text[] text_field = newObj.GetComponentsInChildren<TMP_Text>();
                    Button[] butoane = newObj.GetComponentsInChildren<Button>();
                    butoane[0].gameObject.SetActive(false);

                    text_field[0].text = reader.GetString(3);//nume
                    text_field[1].text = reader.GetString(4);//desc
                                                             //text_field[2].text = reader.GetString(12);//date
                    int gold = reader.GetInt16(7);
                    int exp = reader.GetInt16(6);
                    if (player.pl.clas == 1)
                        exp += 5;
                    if (player.pl.clas == 2)
                        exp -= 5;
                    //text_field[2].text = reader.GetString(5);//date
                    text_field[3].text = "Gold: " + gold + "\nExp: " + exp;//rewards
                    text_field[4].text = reader.GetInt16(0).ToString() + "";//id
                                                                            //data data este depasita atunci Fail(id)
                    int aux = reader.GetInt32(12); //aesthetic
                    if (aux == 1)
                        text_field[5].text = "Physical";
                    if (aux == 2)
                        text_field[5].text = "Mental";
                    if (aux == 3)
                        text_field[5].text = "Emotional";
                    curr_date = DateTime.UtcNow;
                    //date = DateTime.ParseExact(text_field[2].text, "d.M.yyyy", null);
                    Debug.Log("Data Curenta: " + curr_date);
                    Debug.Log("Data Task: " + date);
                    id_task = reader.GetInt16(0);
                }
                //if (curr_date > date)
                //  Fail(id_task);
                reader.Dispose();
                dbcmd.Dispose();
                dbcon.Close();
            }
            else if (i != 0 && i != 5)
            {
                int numberOfTasks = 0;
                SetDb();
                string sql = $"SELECT COUNT(*) FROM todo WHERE type={i + 1} AND player_id={player.pl.id}";
                dbcmd.CommandText = sql;
                reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    numberOfTasks = reader.GetInt16(0);
                }
                reader.Dispose();

                GameObject newObj;
                DateTime curr_date = DateTime.UtcNow, date = DateTime.UtcNow;
                sql = $"SELECT * FROM todo WHERE type={i + 1} AND player_id={player.pl.id}";
                dbcmd.CommandText = sql;
                reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    newObj = (GameObject)Instantiate(prefab, transform);
                    events.Add(newObj);
                    TMP_Text[] text_field = newObj.GetComponentsInChildren<TMP_Text>();
                    Button[] butoane = newObj.GetComponentsInChildren<Button>();
                    butoane[3].gameObject.SetActive(false);
                    butoane[4].gameObject.SetActive(false);

                    text_field[0].text = reader.GetString(3);//nume
                    text_field[1].text = reader.GetString(4);//desc
                    text_field[2].text = reader.GetString(5);//date
                    int gold = reader.GetInt16(7);
                    int exp = reader.GetInt16(6);
                    if (player.pl.clas == 1)
                        exp += 5;
                    if (player.pl.clas == 2)
                        exp -= 5;
                    text_field[3].text = "Gold: " + gold + "\nExp: " + exp;//rewards
                    text_field[4].text = reader.GetInt16(0).ToString() + "";//id
                                                                            //data data este depasita atunci Fail(id)

                    int aux = reader.GetInt32(12); //aesthetic
                    if (aux == 1)
                        text_field[5].text = "Physical";
                    if (aux == 2)
                        text_field[5].text = "Mental";
                    if (aux == 3)
                        text_field[5].text = "Emotional";

                    text_field[6].text = "Remaining: " + reader.GetInt16(11).ToString(); //occurences
                    curr_date = DateTime.UtcNow;
                    date = DateTime.ParseExact(text_field[2].text, "d.M.yyyy", null);
                    Debug.Log("Data Curenta: " + curr_date);
                    Debug.Log("Nume: " + text_field[0].text);
                    id_task = reader.GetInt16(0);
                }
                if (curr_date > date)
                    Fail(id_task, toggles);
                reader.Dispose();
                dbcmd.Dispose();
                dbcon.Close();
            }
            else if (i == 5)
            {
                int numberOfTasks = 0;
                SetDb();
                string sql = $"SELECT COUNT(*) FROM todo WHERE type={i + 1} AND player_id={player.pl.id}";
                dbcmd.CommandText = sql;
                reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    numberOfTasks = reader.GetInt16(0);
                }
                reader.Dispose();

                GameObject newObj;
                DateTime curr_date = DateTime.UtcNow, date = DateTime.UtcNow;
                sql = $"SELECT * FROM todo WHERE type={i + 1} AND player_id={player.pl.id}";
                dbcmd.CommandText = sql;
                reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    newObj = (GameObject)Instantiate(prefab, transform);
                    events.Add(newObj);
                    TMP_Text[] text_field = newObj.GetComponentsInChildren<TMP_Text>();
                    Button[] butoane = newObj.GetComponentsInChildren<Button>();
                    butoane[1].gameObject.SetActive(false);
                    butoane[2].gameObject.SetActive(false);
                    butoane[3].gameObject.SetActive(false);
                    butoane[4].gameObject.SetActive(false);

                    text_field[0].text = reader.GetString(3);//nume
                    text_field[1].text = reader.GetString(4);//desc
                    text_field[2].text = reader.GetString(5);//date
                    int gold = reader.GetInt16(7);
                    int exp = reader.GetInt16(6);
                    if (player.pl.clas == 1)
                        exp += 5;
                    if (player.pl.clas == 2)
                        exp -= 5;
                    text_field[3].text = "Gold: " + gold + "\nExp: " + exp;//rewards
                    text_field[4].text = reader.GetInt16(0).ToString() + "";//id
                                                                 //data data este depasita atunci Fail(id)
                    curr_date = DateTime.UtcNow;
                    date = DateTime.ParseExact(text_field[2].text, "d.M.yyyy", null);
                    Debug.Log("Data Curenta: " + curr_date);
                    Debug.Log("Data Task: " + date);
                    id_task = reader.GetInt16(0);
                }
                if (curr_date > date)
                    Fail(id_task, toggles);
                reader.Dispose();
                dbcmd.Dispose();
                dbcon.Close();
            }
        }
    }
}