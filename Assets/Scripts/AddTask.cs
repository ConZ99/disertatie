using Mono.Data.Sqlite;
using System.Data;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TaskData
{
    public int id, player_id, type = -1, exp, gold, hp_loss, diff;
    public string name, desc;
    public string date;
    public int maxOccurences, remainingOccurences;
    public int aestheticType;
}

public class AddTask : MonoBehaviour
{
    public Player player;
    TaskData task;
    public Toggle[] showToggles;
    public GameObject ui;
    TMP_InputField[] field;
    TMP_Dropdown[] drop;

    int editedId = -1;
    int remainingOccurences = 0;
    int previousMaxOccurences = 0;

    public GameObject buyitm, showitm, buyeff, showeff, taskcont, taskadd;

    public GameObject prefab;
    public PopulateGrid grid;

    public GameObject occurencesButtons;

    string conn;
    IDbConnection dbcon;
    IDbCommand dbcmd;
    IDataReader reader;
    void SetDb()
    {
        conn = "URI=file:" + Application.dataPath + "/db.db";
        dbcon = (IDbConnection)new SqliteConnection(conn);
        dbcon.Open();
        dbcmd = dbcon.CreateCommand();
    }

    //get - afiseaza lista de taskuri sortate dupa type
    //delete - stergerea din db a taskului si get pe type-ul respectiv
    //edit - ecran add in care bag datele din db
    //complete - sterge taskul, get pe type respectiv, primeste recompensa
    //fail - idem, primeste daune

    private void Start()
    {
        task = new TaskData();
        field = ui.GetComponentsInChildren<TMP_InputField>();
        drop = ui.GetComponentsInChildren<TMP_Dropdown>();
        field[1].interactable = false;
        field[3].interactable = false;
        occurencesButtons.SetActive(false);
    }

    public void IncrementOccurence()
    {
        field[3].text = (int.Parse(field[3].text) + 1).ToString();
    }

    public void DecrementOccurence()
    {
        if(int.Parse(field[3].text) > 1)
            field[3].text = (int.Parse(field[3].text) - 1).ToString();
    }

    public void HobbyNoDate()
    {
        if (drop[0].value == 0)
        {
            field[1].interactable = false;
            occurencesButtons.SetActive(false);
        }
        else
        {
            field[1].interactable = true;
            occurencesButtons.SetActive(true);
        }

    }

    public void Cancel()
    {
        buyitm.SetActive(false);
        showitm.SetActive(false);
        buyeff.SetActive(false);
        showeff.SetActive(false);
        taskcont.SetActive(false);
        taskadd.SetActive(false);
        field[0].text = "";
        field[1].text = "";
        field[2].text = "";
        field[3].text = "1";
        drop[0].value = 0;
        drop[1].value = 0;
        drop[2].value = 0;
    }

    public void ShowTasks()
    {
        buyitm.SetActive(false);
        showitm.SetActive(false);
        buyeff.SetActive(false);
        showeff.SetActive(false);
        taskcont.SetActive(true);
        taskadd.SetActive(false);
        if (togglesOn() == 0 || togglesOn() == 6)
            grid.showAll(showToggles);
        else
            grid.showTasks(showToggles);
    }

    private int togglesOn()
    {
        int sum = 0;
        for(int i = 0; i < 6; i++)
        {
            if (showToggles[i].isOn)
                sum = sum + 1;
        }
        return sum;
    }

    public void AddButton()
    {
        buyitm.SetActive(false);
        showitm.SetActive(false);
        buyeff.SetActive(false);
        showeff.SetActive(false);
        taskcont.SetActive(false);
        taskadd.SetActive(true);
        field[0].text = "";
        field[1].text = "";
        field[2].text = "";
        field[3].text = "1";
        drop[0].value = 0;
        drop[1].value = 0;
        drop[2].value = 0;
    }

    public void EditButton(int id)
    {
        buyitm.SetActive(false);
        showitm.SetActive(false);
        buyeff.SetActive(false);
        showeff.SetActive(false);
        taskcont.SetActive(false);
        taskadd.SetActive(true);
        AddDataToFields(id);
    }

    private void AddDataToFields(int id)
    {
        SetDb();
        
        string sql = $"SELECT * FROM todo WHERE id='{id}'";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        while (reader.Read())
        {
            field[0].text = reader.GetString(3);
            field[1].text = reader.GetString(5);
            field[2].text = reader.GetString(4);
            field[3].text = reader.GetInt16(10).ToString();
            previousMaxOccurences = reader.GetInt16(10);
            remainingOccurences = reader.GetInt16(11);
            drop[0].value = reader.GetInt16(2) - 1;
            drop[1].value = reader.GetInt16(9) - 1;
            drop[2].value = reader.GetInt16(9) - 1;
        }
        editedId = id;
        reader = dbcmd.ExecuteReader();
        reader.Dispose();
        dbcmd.Dispose();
        dbcon.Close();
    }

    public void Save()
    {
        Debug.Log(editedId);
        if (editedId != -1)
        {
            SetDb();
            string sql = $"DELETE FROM todo WHERE id='{editedId}'";
            dbcmd.CommandText = sql;
            reader = dbcmd.ExecuteReader();
            reader.Dispose();
            dbcmd.Dispose();
            dbcon.Close();
            editedId = -1;
        }

        task.name = field[0].text;
        task.desc = field[2].text;
        task.date = field[1].text;
        task.maxOccurences = int.Parse(field[3].text);
        if (task.maxOccurences == previousMaxOccurences)
            task.remainingOccurences = remainingOccurences;
        else task.remainingOccurences = task.maxOccurences;
        task.type = drop[0].value + 1;
        task.diff = drop[1].value + 1;
        task.aestheticType = drop[2].value + 1;

        task.player_id = player.pl.id;
        //recompense si pedepse
        task.exp = 10 * task.diff;
        task.gold = 10 * task.diff;
        task.hp_loss = 15 * task.diff;
        Debug.Log(task.type);
        Debug.Log(task.name);
        if (!task.name.Equals("") && !task.desc.Equals("") && task.type != -1)
        {
            var entries = 0;
            Debug.Log("SAVED");
            SetDb();
            string sql = $"SELECT COUNT(*) FROM todo " +
                $"WHERE name='{task.name}' AND player_id={player.pl.id} AND type={task.type}";
            dbcmd.CommandText = sql;
            reader = dbcmd.ExecuteReader();
            while (reader.Read())
                entries = reader.GetInt16(0);
            reader.Dispose();

            if (entries == 0)
            {
                sql = $"INSERT INTO todo(player_id, type, name, desc, date, " +
                      $"exp, gold, hp_loss, diff, maxOccurences, remainingOccurences, aestheticType)" +
                      $" VALUES({task.player_id}, {task.type}, '{task.name}', '{task.desc}', '{task.date}', " +
                      $"{task.exp}, {task.gold}, {task.hp_loss}, {task.diff}, {task.maxOccurences}, {task.remainingOccurences}, {task.aestheticType})";
                dbcmd.CommandText = sql;
                reader = dbcmd.ExecuteReader();
                reader.Dispose();
                //go to show task.type
            }
            dbcmd.Dispose();
            dbcon.Close();
        }
        buyitm.SetActive(false);
        showitm.SetActive(false);
        buyeff.SetActive(false);
        showeff.SetActive(false);
        taskcont.SetActive(false);
        taskadd.SetActive(false);
        //nu se intampla nimic
    }
}
