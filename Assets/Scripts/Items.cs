using Mono.Data.Sqlite;
using System.Collections.Generic;
using System.Data;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class ItemData
{
    public int id;
    public string name;
    public int lv, cost;
    public int ef1_id, ef2_id;
    public string effect1, effect2;
    public int val1, val2;
    public int type;
}
public class Items : MonoBehaviour
{
    //public GameObject shopInterface;
    Button[] buttons_buy;
    Button[] buttons_inv;
    Button[] effs_buy;
    Button[] effs_inv;
    Button[] shop_images_icons, inventory_images_icons;
    private int EOS;
    List<ItemData> items = new List<ItemData>();
    List<EffectsData> effects = new List<EffectsData>();
    public Player player;
    private int shopType = 0;
    private int itemPreviewIndex;
    public GameObject buy, inv, ShopIcons, InventoryIcons;
    public GameObject effects_buy, effects_inv;
    public GameObject taskcont, taskadd;
    public GameObject shopItempreview, inventoryItempreview;
    public GameObject shopItemWindow, inventoryItemWindow;
    public Sprite[] imagini;
    //public GameObject[] screens;
    /*
    1 - boots
    2 - chests
    3 - helmets
    4 - pants
    5 - weapons 
    */

    string conn;
    IDbConnection dbcon;
    IDbCommand dbcmd;
    IDataReader reader;
    public void SetDb()
    {
        conn = "URI=file:" + Application.dataPath + "/db.db";
        dbcon = (IDbConnection)new SqliteConnection(conn);
        dbcon.Open();
        dbcmd = dbcon.CreateCommand();
    }
    private void Start()
    {
        //fac vizibila interfata
        //shopInterface.SetActive(true);
        buy.SetActive(false);
        inv.SetActive(false);
        ShopIcons.SetActive(false);
        InventoryIcons.SetActive(false);
        effects_buy.SetActive(false);
        effects_inv.SetActive(false);
        taskcont.SetActive(false);
        taskadd.SetActive(false);
        shopItemWindow.gameObject.SetActive(false);
        inventoryItemWindow.gameObject.SetActive(false);
        //fac vizibile toate butoanele
        buttons_buy = buy.GetComponentsInChildren<Button>();
        buttons_inv = inv.GetComponentsInChildren<Button>();
        shop_images_icons = ShopIcons.GetComponentsInChildren<Button>();
        inventory_images_icons = InventoryIcons.GetComponentsInChildren<Button>();
        effs_buy = effects_buy.GetComponentsInChildren<Button>();
        effs_inv = effects_inv.GetComponentsInChildren<Button>();
    }

    public void ShowBootsShop()
    {
        EOS = 0;
        shopType = 1;
        shopItemWindow.gameObject.SetActive(false);
        effects_buy.SetActive(false);
        effects_inv.SetActive(false);
        taskcont.SetActive(false);
        taskadd.SetActive(false);
        inv.SetActive(false);
        buy.SetActive(false);
        ShopIcons.SetActive(true);
        InventoryIcons.SetActive(false);
        foreach (Button child in shop_images_icons)
            child.gameObject.SetActive(true);
        items.Clear();

        SetDb();
        //get number of items available
        Debug.Log(player.pl.clas);
        string sql = $"SELECT COUNT(*) FROM shop WHERE type=1 AND amount IS TRUE AND class={player.pl.clas}";
        dbcmd.CommandText = sql;
        IDataReader reader = dbcmd.ExecuteReader();
        int buttonsNumber = 6;
        while (reader.Read())
        {
            buttonsNumber = reader.GetInt16(0);
        }
        reader.Dispose();
        Debug.Log("Numar de butoane: " + buttonsNumber);
        //deactivate other unneeded buttons 
        for (int btn = buttonsNumber; btn < 6; btn++)
        {
            shop_images_icons[btn].gameObject.SetActive(false);
            buttons_buy[btn].gameObject.SetActive(false);
        }

        //populam fiecare buton
        sql = $"SELECT * FROM shop WHERE type=1 AND amount IS TRUE AND class={player.pl.clas}";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        int i = 0;
        while (reader.Read())
        {
            ItemData item = new ItemData();
            item.id = reader.GetInt16(0);
            item.name = reader.GetString(3);
            item.lv = reader.GetInt16(5);
            item.cost = reader.GetInt16(6);

            //schimb textul afisat pt fiecare item
            string text = item.name +
                "\nLevel: " + item.lv +
                "\nPrice: " + item.cost;
            buttons_buy[i].GetComponentInChildren<TMP_Text>().text = text;
            shop_images_icons[i].GetComponent<Image>().sprite = imagini[item.id];
            //retin referinta pentru fiecare item
            items.Add(item);
            i++;
        }
        reader.Dispose();
        dbcmd.Dispose();
        dbcon.Close();
    }

    public void ShowChestsShop()
    {
        EOS = 0;
        shopType = 2;
        shopItemWindow.gameObject.SetActive(false);
        effects_buy.SetActive(false);
        effects_inv.SetActive(false);
        taskcont.SetActive(false);
        taskadd.SetActive(false);
        inv.SetActive(false);
        buy.SetActive(false);
        ShopIcons.SetActive(true);
        InventoryIcons.SetActive(false);
        foreach (Button child in shop_images_icons)
            child.gameObject.SetActive(true);
        items.Clear();

        SetDb();
        //get number of items available
        Debug.Log(player.pl.clas);
        string sql = $"SELECT COUNT(*) FROM shop WHERE type=2 AND amount IS TRUE AND class={player.pl.clas}";
        dbcmd.CommandText = sql;
        IDataReader reader = dbcmd.ExecuteReader();
        int buttonsNumber = 6;
        while (reader.Read())
        {
            buttonsNumber = reader.GetInt16(0);
        }
        reader.Dispose();
        Debug.Log("Numar de butoane: " + buttonsNumber);
        //deactivate other unneeded buttons 
        for (int btn = buttonsNumber; btn < 6; btn++)
        {
            shop_images_icons[btn].gameObject.SetActive(false);
            buttons_buy[btn].gameObject.SetActive(false);
        }

        //populam fiecare buton
        sql = $"SELECT * FROM shop WHERE type=2 AND amount IS TRUE AND class={player.pl.clas}";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        int i = 0;
        while (reader.Read())
        {
            ItemData item = new ItemData();
            item.id = reader.GetInt16(0);
            item.name = reader.GetString(3);
            item.lv = reader.GetInt16(5);
            item.cost = reader.GetInt16(6);

            //schimb textul afisat pt fiecare item
            string text = item.name +
                "\nLevel: " + item.lv +
                "\nPrice: " + item.cost;
            buttons_buy[i].GetComponentInChildren<TMP_Text>().text = text;
            shop_images_icons[i].GetComponent<Image>().sprite = imagini[item.id];
            //retin referinta pentru fiecare item
            items.Add(item);
            i++;
        }
        reader.Dispose();
        dbcmd.Dispose();
        dbcon.Close();
    }

    public void ShowHelmetsShop()
    {
        EOS = 0;
        shopType = 3;
        shopItemWindow.gameObject.SetActive(false);
        effects_buy.SetActive(false);
        effects_inv.SetActive(false);
        taskcont.SetActive(false);
        taskadd.SetActive(false);
        inv.SetActive(false);
        buy.SetActive(false);
        ShopIcons.SetActive(true);
        InventoryIcons.SetActive(false);
        foreach (Button child in shop_images_icons)
            child.gameObject.SetActive(true);
        items.Clear();

        SetDb();
        //get number of items available
        Debug.Log(player.pl.clas);
        string sql = $"SELECT COUNT(*) FROM shop WHERE type=3 AND amount IS TRUE AND class={player.pl.clas}";
        dbcmd.CommandText = sql;
        IDataReader reader = dbcmd.ExecuteReader();
        int buttonsNumber = 6;
        while (reader.Read())
        {
            buttonsNumber = reader.GetInt16(0);
        }
        reader.Dispose();
        Debug.Log("Numar de butoane: " + buttonsNumber);
        //deactivate other unneeded buttons 
        for (int btn = buttonsNumber; btn < 6; btn++)
        {
            shop_images_icons[btn].gameObject.SetActive(false);
            buttons_buy[btn].gameObject.SetActive(false);
        }

        //populam fiecare buton
        sql = $"SELECT * FROM shop WHERE type=3 AND amount IS TRUE AND class={player.pl.clas}";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        int i = 0;
        while (reader.Read())
        {
            ItemData item = new ItemData();
            item.id = reader.GetInt16(0);
            item.name = reader.GetString(3);
            item.lv = reader.GetInt16(5);
            item.cost = reader.GetInt16(6);

            //schimb textul afisat pt fiecare item
            string text = item.name +
                "\nLevel: " + item.lv +
                "\nPrice: " + item.cost;
            buttons_buy[i].GetComponentInChildren<TMP_Text>().text = text;
            shop_images_icons[i].GetComponent<Image>().sprite = imagini[item.id];
            //retin referinta pentru fiecare item
            items.Add(item);
            i++;
        }
        reader.Dispose();
        dbcmd.Dispose();
        dbcon.Close();
    }

    public void ShowPantsShop()
    {
        EOS = 0;
        shopType = 4;
        shopItemWindow.gameObject.SetActive(false);
        effects_buy.SetActive(false);
        effects_inv.SetActive(false);
        taskcont.SetActive(false);
        taskadd.SetActive(false);
        inv.SetActive(false);
        buy.SetActive(false);
        ShopIcons.SetActive(true);
        InventoryIcons.SetActive(false);
        foreach (Button child in shop_images_icons)
            child.gameObject.SetActive(true);
        items.Clear();

        SetDb();
        //get number of items available
        Debug.Log(player.pl.clas);
        string sql = $"SELECT COUNT(*) FROM shop WHERE type=4 AND amount IS TRUE AND class={player.pl.clas}";
        dbcmd.CommandText = sql;
        IDataReader reader = dbcmd.ExecuteReader();
        int buttonsNumber = 6;
        while (reader.Read())
        {
            buttonsNumber = reader.GetInt16(0);
        }
        reader.Dispose();
        Debug.Log("Numar de butoane: " + buttonsNumber);
        //deactivate other unneeded buttons 
        for (int btn = buttonsNumber; btn < 6; btn++)
        {
            shop_images_icons[btn].gameObject.SetActive(false);
            buttons_buy[btn].gameObject.SetActive(false);
        }

        //populam fiecare buton
        sql = $"SELECT * FROM shop WHERE type=4 AND amount IS TRUE AND class={player.pl.clas}";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        int i = 0;
        while (reader.Read())
        {
            ItemData item = new ItemData();
            item.id = reader.GetInt16(0);
            item.name = reader.GetString(3);
            item.lv = reader.GetInt16(5);
            item.cost = reader.GetInt16(6);

            //schimb textul afisat pt fiecare item
            string text = item.name +
                "\nLevel: " + item.lv +
                "\nPrice: " + item.cost;
            buttons_buy[i].GetComponentInChildren<TMP_Text>().text = text;
            shop_images_icons[i].GetComponent<Image>().sprite = imagini[item.id];
            //retin referinta pentru fiecare item
            items.Add(item);
            i++;
        }
        reader.Dispose();
        dbcmd.Dispose();
        dbcon.Close();
    }

    public void ShowWeaponsShop()
    {
        EOS = 0;
        shopType = 5;
        shopItemWindow.gameObject.SetActive(false);
        effects_buy.SetActive(false);
        effects_inv.SetActive(false);
        taskcont.SetActive(false);
        taskadd.SetActive(false);
        inv.SetActive(false);
        buy.SetActive(false);
        ShopIcons.SetActive(true);
        InventoryIcons.SetActive(false);
        foreach (Button child in shop_images_icons)
            child.gameObject.SetActive(true);
        items.Clear();

        SetDb();
        //get number of items available
        Debug.Log(player.pl.clas);
        string sql = $"SELECT COUNT(*) FROM shop WHERE type=5 AND amount IS TRUE AND class={player.pl.clas}";
        dbcmd.CommandText = sql;
        IDataReader reader = dbcmd.ExecuteReader();
        int buttonsNumber = 6;
        while (reader.Read())
        {
            buttonsNumber = reader.GetInt16(0);
        }
        reader.Dispose();
        Debug.Log("Numar de butoane: " + buttonsNumber);
        //deactivate other unneeded buttons 
        for (int btn = buttonsNumber; btn < 6; btn++)
        {
            shop_images_icons[btn].gameObject.SetActive(false);
            buttons_buy[btn].gameObject.SetActive(false);
        }

        //populam fiecare buton
        sql = $"SELECT * FROM shop WHERE type=5 AND amount IS TRUE AND class={player.pl.clas}";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        int i = 0;
        while (reader.Read())
        {
            ItemData item = new ItemData();
            item.id = reader.GetInt16(0);
            item.name = reader.GetString(3);
            item.lv = reader.GetInt16(5);
            item.cost = reader.GetInt16(6);

            //schimb textul afisat pt fiecare item
            string text = item.name +
                "\nLevel: " + item.lv +
                "\nPrice: " + item.cost;
            buttons_buy[i].GetComponentInChildren<TMP_Text>().text = text;
            shop_images_icons[i].GetComponent<Image>().sprite = imagini[item.id];
            //retin referinta pentru fiecare item
            items.Add(item);
            i++;
        }
        reader.Dispose();
        dbcmd.Dispose();
        dbcon.Close();
    }

    public void ShowBootsInv()
    {
        EOS = 1;
        shopType = 1;
        inventoryItemWindow.gameObject.SetActive(false);
        effects_buy.SetActive(false);
        effects_inv.SetActive(false);
        taskcont.SetActive(false);
        taskadd.SetActive(false);
        inv.SetActive(false);
        buy.SetActive(false);
        ShopIcons.SetActive(false);
        InventoryIcons.SetActive(true);
        foreach (Button child in inventory_images_icons)
            child.gameObject.SetActive(true);
        items.Clear();

        SetDb();
        //get number of items available
        //Debug.Log(player.pl.clas);
        string sql = $"SELECT COUNT(*) FROM shop WHERE type=1 AND amount IS FALSE AND class={player.pl.clas}";
        dbcmd.CommandText = sql;
        IDataReader reader = dbcmd.ExecuteReader();
        int buttonsNumber = 6;
        while (reader.Read())
        {
            buttonsNumber = reader.GetInt16(0);
        }
        reader.Dispose();
        //Debug.Log("Numar de butoane: " + buttonsNumber);
        //deactivate other unneeded buttons 
        for (int btn = buttonsNumber; btn < 6; btn++)
        {
            buttons_inv[btn].gameObject.SetActive(false);
            inventory_images_icons[btn].gameObject.SetActive(false);
        }

        //populam fiecare buton
        sql = $"SELECT * FROM shop WHERE type=1 AND amount IS FALSE AND class={player.pl.clas}" +
            $" AND id IN (SELECT id_shop FROM inventory WHERE id_player={player.pl.id})";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        int i = 0;
        while (reader.Read())
        {
            ItemData item = new ItemData();
            item.id = reader.GetInt16(0);
            item.name = reader.GetString(3);
            item.ef1_id = reader.GetInt16(7);
            item.ef2_id = reader.GetInt16(8);
            item.type = reader.GetInt16(1);
            //Debug.Log(reader.GetInt16(0) + " " + reader.GetString(3));

            //schimb textul afisat pt fiecare item
            string text = item.name + "";
            buttons_inv[i].GetComponentInChildren<TMP_Text>().text = text;
            inventory_images_icons[i].GetComponent<Image>().sprite = imagini[item.id];
            //retin referinta pentru fiecare item
            items.Add(item);
            i++;
        }
        reader.Dispose();

        int equippedItem = 0;
        sql = $"SELECT * FROM shop WHERE type=1 AND amount IS FALSE AND class={player.pl.clas}" +
           $" AND id IN (SELECT id_inventory FROM equip WHERE id_player={player.pl.id})";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        while (reader.Read())
        {
            equippedItem = reader.GetInt16(0);
        }
        reader.Dispose();
        Debug.Log("ID item echipat:"+ equippedItem);
        Debug.Log("ID player:" + player.pl.id);
        for (i = 0; i < buttonsNumber; i++)
        {
            if (items[i].id == equippedItem)
                buttons_inv[i].interactable = false;
            else
                buttons_inv[i].interactable = true;
        }
        dbcmd.Dispose();
        dbcon.Close();
    }

    public void ShowChestsInv()
    {
        EOS = 1;
        shopType = 2;
        inventoryItemWindow.gameObject.SetActive(false);
        effects_buy.SetActive(false);
        effects_inv.SetActive(false);
        taskcont.SetActive(false);
        taskadd.SetActive(false);
        inv.SetActive(false);
        buy.SetActive(false);
        ShopIcons.SetActive(false);
        InventoryIcons.SetActive(true);
        foreach (Button child in inventory_images_icons)
            child.gameObject.SetActive(true);
        items.Clear();

        SetDb();
        //get number of items available
        Debug.Log(player.pl.clas);
        string sql = $"SELECT COUNT(*) FROM shop WHERE type=2 AND amount IS FALSE AND class={player.pl.clas}";
        dbcmd.CommandText = sql;
        IDataReader reader = dbcmd.ExecuteReader();
        int buttonsNumber = 6;
        while (reader.Read())
        {
            buttonsNumber = reader.GetInt16(0);
        }
        reader.Dispose();
        Debug.Log("Numar de butoane: " + buttonsNumber);
        //deactivate other unneeded buttons 
        for (int btn = buttonsNumber; btn < 6; btn++)
        {
            buttons_inv[btn].gameObject.SetActive(false);
            inventory_images_icons[btn].gameObject.SetActive(false);
        }

        //populam fiecare buton
        sql = $"SELECT * FROM shop WHERE type=2 AND amount IS FALSE AND class={player.pl.clas}" +
            $" AND id IN (SELECT id_shop FROM inventory WHERE id_player={player.pl.id})";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        int i = 0;
        while (reader.Read())
        {
            ItemData item = new ItemData();
            item.id = reader.GetInt16(0);
            item.name = reader.GetString(3);
            item.ef1_id = reader.GetInt16(7);
            item.ef2_id = reader.GetInt16(8);
            item.type = reader.GetInt16(1);
            Debug.Log(reader.GetInt16(0) + " " + reader.GetString(3));

            //schimb textul afisat pt fiecare item
            string text = item.name + "";
            buttons_inv[i].GetComponentInChildren<TMP_Text>().text = text;
            inventory_images_icons[i].GetComponent<Image>().sprite = imagini[item.id];
            //retin referinta pentru fiecare item
            items.Add(item);
            i++;
        }
        reader.Dispose();

        int equippedItem = 0;
        sql = $"SELECT * FROM shop WHERE type=2 AND amount IS FALSE AND class={player.pl.clas}" +
           $" AND id IN (SELECT id_inventory FROM equip WHERE id_player={player.pl.id})";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        while (reader.Read())
        {
            equippedItem = reader.GetInt16(0);
        }
        reader.Dispose();
        Debug.Log("AAAAAAAAAAAAAA:" + equippedItem);
        for (i = 0; i < buttonsNumber; i++)
        {
            if (items[i].id == equippedItem)
                buttons_inv[i].interactable = false;
            else
                buttons_inv[i].interactable = true;
        }
        dbcmd.Dispose();
        dbcon.Close();
    }

    public void ShowHelmetsInv()
    {
        EOS = 1;
        shopType = 3;
        inventoryItemWindow.gameObject.SetActive(false);
        effects_buy.SetActive(false);
        effects_inv.SetActive(false);
        taskcont.SetActive(false);
        taskadd.SetActive(false);
        inv.SetActive(false);
        buy.SetActive(false);
        ShopIcons.SetActive(false);
        InventoryIcons.SetActive(true);
        foreach (Button child in inventory_images_icons)
            child.gameObject.SetActive(true);
        items.Clear();

        SetDb();
        //get number of items available
        Debug.Log(player.pl.clas);
        string sql = $"SELECT COUNT(*) FROM shop WHERE type=3 AND amount IS FALSE AND class={player.pl.clas}";
        dbcmd.CommandText = sql;
        IDataReader reader = dbcmd.ExecuteReader();
        int buttonsNumber = 6;
        while (reader.Read())
        {
            buttonsNumber = reader.GetInt16(0);
        }
        reader.Dispose();
        Debug.Log("Numar de butoane: " + buttonsNumber);
        //deactivate other unneeded buttons 
        for (int btn = buttonsNumber; btn < 6; btn++)
        {
            buttons_inv[btn].gameObject.SetActive(false);
            inventory_images_icons[btn].gameObject.SetActive(false);
        }

        //populam fiecare buton
        sql = $"SELECT * FROM shop WHERE type=3 AND amount IS FALSE AND class={player.pl.clas}" +
            $" AND id IN (SELECT id_shop FROM inventory WHERE id_player={player.pl.id})";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        int i = 0;
        while (reader.Read())
        {
            ItemData item = new ItemData();
            item.id = reader.GetInt16(0);
            item.name = reader.GetString(3);
            item.ef1_id = reader.GetInt16(7);
            item.ef2_id = reader.GetInt16(8);
            item.type = reader.GetInt16(1);
            Debug.Log(reader.GetInt16(0) + " " + reader.GetString(3));

            //schimb textul afisat pt fiecare item
            string text = item.name + "";
            buttons_inv[i].GetComponentInChildren<TMP_Text>().text = text;
            inventory_images_icons[i].GetComponent<Image>().sprite = imagini[item.id];
            //retin referinta pentru fiecare item
            items.Add(item);
            i++;
        }
        reader.Dispose();

        int equippedItem = 0;
        sql = $"SELECT * FROM shop WHERE type=3 AND amount IS FALSE AND class={player.pl.clas}" +
           $" AND id IN (SELECT id_inventory FROM equip WHERE id_player={player.pl.id})";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        while (reader.Read())
        {
            equippedItem = reader.GetInt16(0);
        }
        reader.Dispose();
        Debug.Log("AAAAAAAAAAAAAA:" + equippedItem);
        for (i = 0; i < buttonsNumber; i++)
        {
            if (items[i].id == equippedItem)
                buttons_inv[i].interactable = false;
            else
                buttons_inv[i].interactable = true;
        }
        dbcmd.Dispose();
        dbcon.Close();
    }

    public void ShowPantsInv()
    {
        EOS = 1;
        shopType = 4;
        inventoryItemWindow.gameObject.SetActive(false);
        effects_buy.SetActive(false);
        effects_inv.SetActive(false);
        taskcont.SetActive(false);
        taskadd.SetActive(false);
        inv.SetActive(false);
        buy.SetActive(false);
        ShopIcons.SetActive(false);
        InventoryIcons.SetActive(true);
        foreach (Button child in inventory_images_icons)
            child.gameObject.SetActive(true);
        items.Clear();

        SetDb();
        //get number of items available
        Debug.Log(player.pl.clas);
        string sql = $"SELECT COUNT(*) FROM shop WHERE type=4 AND amount IS FALSE AND class={player.pl.clas}";
        dbcmd.CommandText = sql;
        IDataReader reader = dbcmd.ExecuteReader();
        int buttonsNumber = 6;
        while (reader.Read())
        {
            buttonsNumber = reader.GetInt16(0);
        }
        reader.Dispose();
        Debug.Log("Numar de butoane: " + buttonsNumber);
        //deactivate other unneeded buttons 
        for (int btn = buttonsNumber; btn < 6; btn++)
        {
            buttons_inv[btn].gameObject.SetActive(false);
            inventory_images_icons[btn].gameObject.SetActive(false);
        }

        //populam fiecare buton
        sql = $"SELECT * FROM shop WHERE type=4 AND amount IS FALSE AND class={player.pl.clas}" +
            $" AND id IN (SELECT id_shop FROM inventory WHERE id_player={player.pl.id})";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        int i = 0;
        while (reader.Read())
        {
            ItemData item = new ItemData();
            item.id = reader.GetInt16(0);
            item.name = reader.GetString(3);
            item.ef1_id = reader.GetInt16(7);
            item.ef2_id = reader.GetInt16(8);
            item.type = reader.GetInt16(1);
            Debug.Log(reader.GetInt16(0) + " " + reader.GetString(3));

            //schimb textul afisat pt fiecare item
            string text = item.name + "";
            buttons_inv[i].GetComponentInChildren<TMP_Text>().text = text;
            inventory_images_icons[i].GetComponent<Image>().sprite = imagini[item.id];
            //retin referinta pentru fiecare item
            items.Add(item);
            i++;
        }
        reader.Dispose();

        int equippedItem = 0;
        sql = $"SELECT * FROM shop WHERE type=4 AND amount IS FALSE AND class={player.pl.clas}" +
           $" AND id IN (SELECT id_inventory FROM equip WHERE id_player={player.pl.id})";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        while (reader.Read())
        {
            equippedItem = reader.GetInt16(0);
        }
        reader.Dispose();
        Debug.Log("AAAAAAAAAAAAAA:" + equippedItem);
        for (i = 0; i < buttonsNumber; i++)
        {
            if (items[i].id == equippedItem)
                buttons_inv[i].interactable = false;
            else
                buttons_inv[i].interactable = true;
        }
        dbcmd.Dispose();
        dbcon.Close();
    }

    public void ShowWeaponsInv()
    {
        EOS = 1;
        shopType = 5;
        inventoryItemWindow.gameObject.SetActive(false);
        effects_buy.SetActive(false);
        effects_inv.SetActive(false);
        taskcont.SetActive(false);
        taskadd.SetActive(false);
        inv.SetActive(false);
        buy.SetActive(false);
        ShopIcons.SetActive(false);
        InventoryIcons.SetActive(true);
        foreach (Button child in inventory_images_icons)
            child.gameObject.SetActive(true);
        items.Clear();

        SetDb();
        //get number of items available
        Debug.Log(player.pl.clas);
        string sql = $"SELECT COUNT(*) FROM shop WHERE type=5 AND amount IS FALSE AND class={player.pl.clas}";
        dbcmd.CommandText = sql;
        IDataReader reader = dbcmd.ExecuteReader();
        int buttonsNumber = 6;
        while (reader.Read())
        {
            buttonsNumber = reader.GetInt16(0);
        }
        reader.Dispose();
        Debug.Log("Numar de butoane: " + buttonsNumber);
        //deactivate other unneeded buttons 
        for (int btn = buttonsNumber; btn < 6; btn++)
        {
            buttons_inv[btn].gameObject.SetActive(false);
            inventory_images_icons[btn].gameObject.SetActive(false);
        }

        //populam fiecare buton
        sql = $"SELECT * FROM shop WHERE type=5 AND amount IS FALSE AND class={player.pl.clas}" +
            $" AND id IN (SELECT id_shop FROM inventory WHERE id_player={player.pl.id})";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        int i = 0;
        while (reader.Read())
        {
            ItemData item = new ItemData();
            item.id = reader.GetInt16(0);
            item.name = reader.GetString(3);
            item.ef1_id = reader.GetInt16(7);
            item.ef2_id = reader.GetInt16(8);
            item.type = reader.GetInt16(1);
            Debug.Log(reader.GetInt16(0) + " " + reader.GetString(3));

            //schimb textul afisat pt fiecare item
            string text = item.name + "";
            buttons_inv[i].GetComponentInChildren<TMP_Text>().text = text;
            inventory_images_icons[i].GetComponent<Image>().sprite = imagini[item.id];
            //retin referinta pentru fiecare item
            items.Add(item);
            i++;
        }
        reader.Dispose();

        int equippedItem = 0;
        sql = $"SELECT * FROM shop WHERE type=5 AND amount IS FALSE AND class={player.pl.clas}" +
           $" AND id IN (SELECT id_inventory FROM equip WHERE id_player={player.pl.id})";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        while (reader.Read())
        {
            equippedItem = reader.GetInt16(0);
        }
        reader.Dispose();
        Debug.Log("AAAAAAAAAAAAAA:" + equippedItem);
        for (i = 0; i < buttonsNumber; i++)
        {
            if (items[i].id == equippedItem)
                buttons_inv[i].interactable = false;
            else
                buttons_inv[i].interactable = true;
        }
        dbcmd.Dispose();
        dbcon.Close();
    }

    public void Equip()
    {
        SetDb();
        //iau butoanele si verific textul pt a vedea la ce buton ma aflu
        int index = itemPreviewIndex;

        //caut in shop detaliile efectelor itemului
        //iau primul efect
        string sql = $"SELECT * FROM effects WHERE id={items[index].ef1_id}";
        dbcmd.CommandText = sql;
        IDataReader reader = dbcmd.ExecuteReader();
        while (reader.Read())
        {
            items[index].effect1 = reader.GetString(1);
            items[index].val1 = reader.GetInt16(2);
        }
        reader.Dispose();
        //iau al doilea efect
        sql = $"SELECT * FROM effects WHERE id={items[index].ef2_id}";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        while (reader.Read())
        {
            items[index].effect2 = reader.GetString(1);
            items[index].val2 = reader.GetInt16(2);
        }
        reader.Dispose();

        inventoryItemWindow.gameObject.SetActive(false);

        //ia detaliile vechiului item
        
        sql = $"SELECT * FROM shop WHERE id IN " +
            $"(SELECT id_inventory FROM equip WHERE id_player={player.pl.id}) AND type={items[index].type}";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();

        ItemData aux_itm = new ItemData();
        while (reader.Read())
        {
            aux_itm.id = reader.GetInt16(0);
            aux_itm.name = reader.GetString(3);
            aux_itm.ef1_id = reader.GetInt16(7);
            aux_itm.ef2_id = reader.GetInt16(8);
        }
        reader.Dispose();
        if (aux_itm.id != 0)
        {
            //iau primul efect
            sql = $"SELECT * FROM effects WHERE id={aux_itm.ef1_id}";
            dbcmd.CommandText = sql;
            reader = dbcmd.ExecuteReader();
            while (reader.Read())
            {
                aux_itm.effect1 = reader.GetString(1);
                aux_itm.val1 = reader.GetInt16(2);
            }
            reader.Dispose();
            //iau al doilea efect
            sql = $"SELECT * FROM effects WHERE id={aux_itm.ef2_id}";
            dbcmd.CommandText = sql;
            reader = dbcmd.ExecuteReader();
            while (reader.Read())
            {
                aux_itm.effect2 = reader.GetString(1);
                aux_itm.val2 = reader.GetInt16(2);
            }
            reader.Dispose();
        }

        Debug.Log(aux_itm.name + " --- " + aux_itm.id);
        Debug.Log(items[index].name);
        //update in equip cu ID-ul nou
        if (aux_itm.id != 0)
            sql = $"UPDATE equip SET id_inventory={items[index].id} WHERE id_inventory={aux_itm.id}";
        else
            sql = $"INSERT INTO equip(id_player, id_inventory) VALUES({player.pl.id}, {items[index].id})";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        reader.Dispose();
        dbcmd.Dispose();
        dbcon.Close();

        //update player stats
        player.pl.hp += items[index].val1 + items[index].val2 - aux_itm.val1 - aux_itm.val2;
        player.UpdateStats();

        if (shopType == 1)
        {
            ShowBootsInv();
        }
        else if (shopType == 2)
        {
            ShowChestsInv();
        }
        else if (shopType == 3)
        {
            ShowHelmetsInv();
        }
        else if (shopType == 4)
        {
            ShowPantsInv();
        }
        else
            ShowWeaponsInv();
    }

    public void Buy()
    {
        SetDb();
        //iau butoanele si verific textul pt a vedea la ce buton ma aflu
        int index = itemPreviewIndex;

        if(player.pl.lv >= items[index].lv)
            if(player.pl.gold >= items[index].cost)
            {
                player.pl.gold -= items[index].cost;
                player.UpdateStats();
                
                //scot din shop
                string sql = $"UPDATE shop SET amount = FALSE WHERE id={items[index].id}";
                dbcmd.CommandText = sql;
                IDataReader reader = dbcmd.ExecuteReader();
                reader.Dispose();

                //adauga in inventory
                Debug.Log(player.pl.id);
                Debug.Log(items[index].id);
                Debug.Log(items[index].name);
                sql = $"INSERT INTO inventory(id_player, id_shop) VALUES({player.pl.id}, {items[index].id})";
                dbcmd.CommandText = sql;
                reader = dbcmd.ExecuteReader();
                reader.Dispose();

                shopItemWindow.gameObject.SetActive(false);

                player.pl.previousAchievementValue = player.pl.achievementTracker[6];
                player.pl.achievementTracker[6]++;
                player.incrementAchievement(6);

                if (shopType == 1)
                {
                    ShowBootsShop();
                }
                else if (shopType == 2)
                {
                    ShowChestsShop();
                }
                else if (shopType == 3)
                {
                    ShowHelmetsShop();
                }
                else if (shopType == 4)
                {
                    ShowPantsShop();
                }
                else
                    ShowWeaponsShop();//call function based on shopType;
            }
        dbcmd.Dispose();
        dbcon.Close();
        //buy helmet, equip/unequip helmet, update status
    }

    public void Preview()
    {
        Button[] aux;
        if (EOS == 0)
        {
            shopItemWindow.gameObject.SetActive(true);
            aux = ShopIcons.GetComponentsInChildren<Button>();
        }
        else
        {
            inventoryItemWindow.gameObject.SetActive(true);
            aux = InventoryIcons.GetComponentsInChildren<Button>();
        }
        //SetDb();
        //iau butoanele si verific textul pt a vedea la ce buton ma aflu
        int index = 0;
        int IOE = 0;
        GameObject asd = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject;
        if (EOS == 0)
            shopItempreview.GetComponent<Image>().sprite = asd.GetComponent<Image>().sprite;
        else inventoryItempreview.GetComponent<Image>().sprite = asd.GetComponent<Image>().sprite;


        for (int i = 0; i < aux.Length; i++)
        {
            if (asd.GetComponent<Image>().sprite.Equals(aux[i].GetComponent<Image>().sprite))
            {
                index = i;
                break;
            }
        }

        for (int i = 1; i < imagini.Length; i++)
        {
            if (asd.GetComponent<Image>().sprite.Equals(imagini[i]))
            {
                IOE = i;
                break;
            }
        }

        if (EOS == 0)
        {
            if (IOE < 91)
            {
                buy.SetActive(true);
                for (int i = 0; i < aux.Length; i++)
                {
                    buttons_buy[i].gameObject.SetActive(false);
                }

                buttons_buy[index].gameObject.SetActive(true);
            }
            else
            {
                effects_buy.SetActive(true);
                for (int i = 0; i < aux.Length; i++)
                {
                    effs_buy[i].gameObject.SetActive(false);
                }

                effs_buy[index].gameObject.SetActive(true);
            }
        }
        else
        {
            if (IOE < 91)
            {
                inv.SetActive(true);
                for (int i = 0; i < aux.Length; i++)
                {
                    buttons_inv[i].gameObject.SetActive(false);
                }

                buttons_inv[index].gameObject.SetActive(true);
            }
            else
            {
                effects_inv.SetActive(true);
                for (int i = 0; i < aux.Length; i++)
                {
                    effs_inv[i].gameObject.SetActive(false);
                }

                effs_inv[index].gameObject.SetActive(true);
            }
        }
        itemPreviewIndex = index;
    }

    public class EffectsData
    {
        public int id;
        public string name;
        public int val, cost;
    }
    public void ShowEffectsShop()
    {
        EOS = 0;
        shopItemWindow.gameObject.SetActive(false);
        buy.SetActive(false);
        inv.SetActive(false);
        taskcont.SetActive(false);
        taskadd.SetActive(false);
        effects_inv.SetActive(false);
        effects_buy.SetActive(false);
        ShopIcons.SetActive(true);
        InventoryIcons.SetActive(false);
        Debug.Log(effs_buy.Length + " atatea butoane");
        foreach (Button child in shop_images_icons)
            child.gameObject.SetActive(true);
        effects.Clear();

        SetDb();
        //get number of items available
        string sql = $"SELECT COUNT(*) FROM effects WHERE amount IS TRUE AND id >= 8 AND id <= 12";
        dbcmd.CommandText = sql;
        IDataReader reader = dbcmd.ExecuteReader();
        int buttonsNumber = 6;
        while (reader.Read())
        {
            buttonsNumber = reader.GetInt16(0);
        }
        reader.Dispose();
        Debug.Log("Numar de butoane: " + buttonsNumber);
        //deactivate other unneeded buttons 
        for (int btn = buttonsNumber; btn < 6; btn++)
        {
            effs_buy[btn].gameObject.SetActive(false);
            shop_images_icons[btn].gameObject.SetActive(false);
        }

        //populam fiecare buton
        sql = $"SELECT * FROM effects WHERE amount IS TRUE AND id >= 8 AND id <= 12";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        int i = 0;
        while (reader.Read())
        {
            EffectsData effect = new EffectsData();
            effect.id = reader.GetInt16(0);
            effect.name = reader.GetString(1);
            effect.cost = reader.GetInt16(4);
            effect.val = reader.GetInt16(2);

            //schimb textul afisat pt fiecare item
            string text = effect.name +
                "\nPrice: " + effect.cost;
            effs_buy[i].GetComponentInChildren<TMP_Text>().text = text;
            shop_images_icons[i].GetComponent<Image>().sprite = imagini[83 + effect.id];
            //retin referinta pentru fiecare item
            effects.Add(effect);
            i++;
        }
        reader.Dispose();
        dbcmd.Dispose();
        dbcon.Close();
    }

    public void ShowEffectsInv()
    {
        EOS = 1;
        inventoryItemWindow.gameObject.SetActive(false);
        taskcont.SetActive(false);
        taskadd.SetActive(false);
        effects_inv.SetActive(false);
        effects_buy.SetActive(false);
        ShopIcons.SetActive(false);
        InventoryIcons.SetActive(true);
        Debug.Log(effs_buy.Length + " atatea butoane");
        foreach (Button child in inventory_images_icons)
            child.gameObject.SetActive(true);
        effects.Clear();

        SetDb();
        //get number of items available
        string sql = $"SELECT COUNT(*) FROM effects_inventory WHERE id_player={player.pl.id}" +
            $" AND id_effect >= 8 AND id_effect <= 12";
        dbcmd.CommandText = sql;
        IDataReader reader = dbcmd.ExecuteReader();
        int buttonsNumber = 6;
        while (reader.Read())
        {
            buttonsNumber = reader.GetInt16(0);
        }
        reader.Dispose();
        Debug.Log("Numar de butoane: " + buttonsNumber);
        //deactivate other unneeded buttons 
        for (int btn = buttonsNumber; btn < 6; btn++)
        {
            effs_inv[btn].gameObject.SetActive(false);
            inventory_images_icons[btn].gameObject.SetActive(false);
        }

        //populam fiecare buton
        sql = $"SELECT * FROM effects WHERE amount IS FALSE AND id >= 8 AND id <= 12" +
            $" AND id IN (SELECT id_effect FROM effects_inventory WHERE id_player={player.pl.id})";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        int i = 0;
        while (reader.Read())
        {
            EffectsData effect = new EffectsData();
            effect.id = reader.GetInt16(0);
            effect.name = reader.GetString(1);
            effect.cost = reader.GetInt16(4);
            effect.val = reader.GetInt16(2);

            //schimb textul afisat pt fiecare item
            string text = effect.name + "\nEffect: " + effect.val;
            Debug.Log(text);
            effs_inv[i].GetComponentInChildren<TMP_Text>().text = text;
            inventory_images_icons[i].GetComponent<Image>().sprite = imagini[91 + i];
            //retin referinta pentru fiecare item
            effects.Add(effect);
            i++;
        }
        reader.Dispose();

        int equippedItem = 0;
        sql = $"SELECT * FROM effects WHERE amount IS FALSE AND id >= 8 AND id <= 12" +
           $" AND id IN (SELECT id_effect_inventory FROM equip WHERE id_player={player.pl.id})";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        while (reader.Read())
        {
            equippedItem = reader.GetInt16(0);
        }
        reader.Dispose();
        for (i = 0; i < buttonsNumber; i++)
        {
            if (effects[i].id == equippedItem)
                effs_inv[i].interactable = false;
            else
                effs_inv[i].interactable = true;
        }
        dbcmd.Dispose();
        dbcon.Close();
    }

    public void BuyEffect()
    {
        SetDb();
        //iau butoanele si verific textul pt a vedea la ce buton ma aflu
        int index = itemPreviewIndex;
        if (player.pl.gold >= effects[index].cost)
        {
            player.pl.gold -= effects[index].cost;
            player.UpdateStats();
            //scot din shop
            string sql = $"UPDATE effects SET amount = FALSE WHERE id={effects[index].id}";
            dbcmd.CommandText = sql;
            IDataReader reader = dbcmd.ExecuteReader();
            reader.Dispose();

            //adauga in inventory
            sql = $"INSERT INTO effects_inventory(id_player, id_effect) VALUES({player.pl.id}, {effects[index].id})";
            dbcmd.CommandText = sql;
            reader = dbcmd.ExecuteReader();
            reader.Dispose();
            dbcmd.Dispose();
            dbcon.Close();

            player.pl.previousAchievementValue = player.pl.achievementTracker[7];
            player.pl.achievementTracker[7]++;
            player.incrementAchievement(7);

            ShowEffectsShop();
        }
        //buy helmet, equip/unequip helmet, update status
    }

    public void EquipEffect()
    {
        SetDb();
        Debug.Log("EQUIPPED");
        //iau butoanele si verific textul pt a vedea la ce buton ma aflu
        int index = itemPreviewIndex;

        //ia detaliile vechiului item
        EffectsData aux_itm = new EffectsData();
        string sql = $"SELECT * FROM effects WHERE id IN (SELECT id_effect_inventory FROM equip WHERE id_player={player.pl.id})";
        dbcmd.CommandText = sql;
        IDataReader reader = dbcmd.ExecuteReader();
        while (reader.Read())
        {
            aux_itm.id = reader.GetInt16(0);
            aux_itm.name = reader.GetString(1);
            aux_itm.cost = reader.GetInt16(4);
            aux_itm.val = reader.GetInt16(2);
        }
        reader.Dispose();
        Debug.Log("NUME EFECT: " + aux_itm.name);
        Debug.Log("NUME EFECT NOU: " + effects[index].name);

        //update in equip cu ID-ul nou
        if (aux_itm.id != 0)
            sql = $"UPDATE equip SET id_effect_inventory={effects[index].id} WHERE id_effect_inventory={aux_itm.id}";
        else
            sql = $"INSERT INTO equip(id_player, id_effect_inventory) VALUES({player.pl.id}, {effects[index].id})";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        reader.Dispose();
        dbcmd.Dispose();
        dbcon.Close();

        //update player stats
        player.pl.hp += effects[index].val - aux_itm.val;
        player.pl.effect_desc = effects[index].name;
        player.pl.effect_value = effects[index].val;
        player.UpdateStats();
        //buy helmet, equip/unequip helmet, update status

        ShowEffectsInv();
    }
}
