using Mono.Data.Sqlite;
using System;
using System.Data;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections.Generic;

public class PlayerData
{
    public int id, clas, lv, gold, hp, exp;
    public string name;
    public int effect_id, weapon_id, helmet_id, chest_id, pants_id, boots_id;
    public string effect_desc = "";
    public int effect_value = 0;

    public int[] achievementTracker = new int[8];

    public int previousAchievementValue = 0;
}
public class Player : MonoBehaviour
{
    public TMP_Text name;
    public TMP_Text hp;
    public TMP_Text lv;
    public TMP_Text exp;
    public TMP_Text pl_class;
    public TMP_Text gold;
    public TMP_Text eff_desc;
    public TMP_Text eff_value;
    public HealthBar healthBar;
    public ExpBar expBar;
    public Image warrior, monk, mage;

    public TMP_Text[] achievementsRequirements;
    public GameObject achievementsScreen;

    public PlayerData pl = new PlayerData();
    string conn;
    IDbConnection dbcon;
    IDbCommand dbcmd;
    IDataReader reader;
    public void SetDb()
    {
        conn = "URI=file:" + Application.dataPath + "/db.db";
        dbcon = (IDbConnection)new SqliteConnection(conn);
        dbcon.Open();
        dbcmd = dbcon.CreateCommand();
    }
    void Start()
    {
        SetUpPlayer();
        UpdateStats();
        ResetUserItems();
    }

    public void Logout()
    {
        SceneManager.LoadScene(0);
    }

    void ResetUserItems()
    {
        ResetShop();
        ResetEffects();
    }

    void ResetEffects()
    {
        SetDb();
        //check effects
        string sql = $"UPDATE effects SET amount=0";
        dbcmd.CommandText = sql;
        IDataReader reader = dbcmd.ExecuteReader();
        reader.Dispose();

        sql = $"UPDATE effects SET amount=1 WHERE effects.id NOT IN " +
           $"(SELECT id_effect FROM effects_inventory WHERE {pl.id}=id_player AND id_effect >=8 AND id_effect <= 12)" +
           $"AND effects.id <= 12 AND effects.id >= 8";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        reader.Dispose();
        dbcmd.Dispose();
        dbcon.Close();
    }

    void ResetShop()
    {
        SetDb();
        //check items
        string sql = $"UPDATE shop SET amount=1";
        dbcmd.CommandText = sql;
        IDataReader reader = dbcmd.ExecuteReader();
        reader.Dispose();

        sql = $"UPDATE shop SET amount=0 WHERE shop.id IN " +
           $"(SELECT id_shop FROM inventory WHERE {pl.id}=id_player)";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        reader.Dispose();
        dbcmd.Dispose();
        dbcon.Close();
    }

    void SetUpPlayer()
    {
        SetDb();

        name.text = PlayerPrefs.GetString("name"); //dev purposes
        pl.name = PlayerPrefs.GetString("name");
        //pl.name = "Stefan";


        string sql = $"SELECT * FROM player WHERE name='{pl.name}'";
        dbcmd.CommandText = sql;
        IDataReader reader = dbcmd.ExecuteReader();
        while (reader.Read())
        {
            pl.id = reader.GetInt16(0);
            Debug.Log(pl.id);
            pl.name = reader.GetString(1);
            Debug.Log(pl.name);
            pl.clas = reader.GetInt16(2);
            Debug.Log(pl.clas);
            pl.lv = reader.GetInt16(3);
            Debug.Log(pl.lv);
            pl.gold = reader.GetInt16(4);
            Debug.Log(pl.gold);
            pl.hp = reader.GetInt16(5);
            Debug.Log(pl.hp);
            pl.exp = reader.GetInt16(6);
            Debug.Log(pl.exp);

            pl.achievementTracker[0] = reader.GetInt16(7);
            pl.achievementTracker[1] = reader.GetInt16(8);
            pl.achievementTracker[2] = reader.GetInt16(9);
            pl.achievementTracker[3] = reader.GetInt16(10);
            pl.achievementTracker[4] = reader.GetInt16(11);
            pl.achievementTracker[5] = reader.GetInt16(12);
            pl.achievementTracker[6] = reader.GetInt16(13);
            pl.achievementTracker[7] = reader.GetInt16(14);
        }
        reader.Dispose();
        dbcmd.Dispose();
        dbcon.Close();
    }

    public void achievementReward(int limitMin, int limitAverage, int limitMax, int index)
    {
        if (pl.previousAchievementValue < limitMax && pl.achievementTracker[index] >= limitMax)
        {
            pl.exp += 50;
        }
        else if (pl.previousAchievementValue < limitAverage && pl.achievementTracker[index] >= limitAverage)
        {
            pl.exp += 30;
        }
        else if(pl.previousAchievementValue < limitMin && pl.achievementTracker[index] >= limitMin) 
        {
            pl.exp += 15;
        }
    }

    public void incrementAchievement(int type)
    {
        SetDb();
        string sql = "";
        if (type == 0)
        {
            sql = $"UPDATE player SET totalEarnedGold={pl.achievementTracker[0]} WHERE id={pl.id}";
            achievementReward(100, 200, 1000, 0);
        }
        else if(type == 1)
        {
            sql = $"UPDATE player SET totalCompletedHobbies={pl.achievementTracker[1]} WHERE id={pl.id}";
            achievementReward(10, 20, 30, 1);
        }
        else if(type == 2)
        {
            sql = $"UPDATE player SET totalCompletedDailies={pl.achievementTracker[3]} WHERE id={pl.id}";
            achievementReward(10, 20, 30, 3);
        }
        else if(type == 3)
        {
            sql = $"UPDATE player SET totalCompletedWeekly={pl.achievementTracker[4]} WHERE id={pl.id}";
            achievementReward(5, 10, 20, 4);
        }
        else if(type == 4)
        {
            sql = $"UPDATE player SET totalCompletedMonthly={pl.achievementTracker[5]} WHERE id={pl.id}";
            achievementReward(3, 6, 9, 5);
        }
        else if(type == 5)
        {
            sql = $"UPDATE player SET totalCompletedTodos={pl.achievementTracker[2]} WHERE id={pl.id}";
            achievementReward(10, 20, 30, 2);
        }
        else if(type == 6)
        {
            sql = $"UPDATE player SET totalBoughtItems={pl.achievementTracker[6]} WHERE id={pl.id}";
            achievementReward(10, 20, 30, 6);
        }
        else if (type == 7)
        {
            sql = $"UPDATE player SET totalBoughtEffects={pl.achievementTracker[7]} WHERE id={pl.id}";
            achievementReward(1, 3, 5, 7);
        }

        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        reader.Dispose();
    }

    

    public void UpdateStats()
    {
        SetDb();
        AlterPlayerHp();
        AlterPlayerExp();
        AlterPlayerLv();
        AlterPlayerGold();
        name.text = pl.name;
        hp.text = "" + pl.hp;
        exp.text = "" + pl.exp + "/100";
        lv.text = "" + pl.lv;
        expBar.SetMaxExp(pl.exp);
        if (pl.clas == 1)
        {
            mage.enabled = true;
            warrior.enabled = false;
            monk.enabled = false;
        }
        if (pl.clas == 2)
        {
            warrior.enabled = true;
            monk.enabled = false;
            mage.enabled = false;
        }
        if (pl.clas == 3)
        {
            monk.enabled = true;
            mage.enabled = false;
            warrior.enabled = false;
        }
        gold.text = "" + pl.gold;
        if (pl.effect_value != 0)
        {
            eff_desc.text = pl.effect_desc;
            eff_value.text = pl.effect_value + "+ HP";
        }
    }

    private void AlterPlayerGold()
    {
        string sql = $"UPDATE player SET gold={pl.gold} WHERE id={pl.id}";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        reader.Dispose();
    }

    private void AlterPlayerLv()
    {
        string sql = $"UPDATE player SET level={pl.lv} WHERE id={pl.id}";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        reader.Dispose();
    }

    private void AlterPlayerExp()
    {
        string sql = $"UPDATE player SET exp={pl.exp} WHERE id={pl.id}";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        reader.Dispose();
    }

    private void AlterPlayerHp()
    {
        string sql = $"UPDATE player SET hp={pl.hp} WHERE id={pl.id}";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        reader.Dispose();
    }

    public void AddExp()
    {
        pl.exp += 100;
        pl.gold += 100;
        UpdateStats();
        if (pl.exp >= 100)
        {
            //la levelup schimba in baza de date
            pl.exp -= 100;
            pl.lv++;
            pl.hp += 10;
            UpdateStats();
            CheckEffects();
            CheckEvents();
        }
    }

    public void AddRewards(int exp, int gold, int hp)
    {
        pl.exp += exp;
        pl.gold += gold;
        pl.hp -= hp;
        UpdateStats();
        if (pl.hp <= 0)
        {
            Gameover();
        }
        else if (pl.exp >= 100)
        {
            pl.exp -= 100;
            pl.lv++;
            pl.hp += 10;
            UpdateStats();
            CheckEffects();
            CheckEvents();
        }
    }

    void CheckEffects()
    {
        string sql = $"SELECT id FROM effects WHERE {pl.lv}=effect AND id <= 7";
        dbcmd.CommandText = sql;
        IDataReader reader = dbcmd.ExecuteReader();
        int effect_id = 0;
        while (reader.Read())
        {
            effect_id = reader.GetInt16(0);
        }
        reader.Dispose();

        if (effect_id != 0)
        {
            sql = $"INSERT INTO effects_inventory(id_player, id_effect) VALUES ({pl.id}, {effect_id})";
            dbcmd.CommandText = sql;
            reader = dbcmd.ExecuteReader();
            reader.Dispose();
        }
        dbcmd.Dispose();
        dbcon.Close();
    }

    void CheckEvents()
    {
        SetDb();
        string sql = $"SELECT * FROM events WHERE {pl.lv}=level";
        string name = "", desc = "", date = "";
        int exp = 0, gold = 0, diff = 0;
        dbcmd.CommandText = sql;
        IDataReader reader = dbcmd.ExecuteReader();
        while (reader.Read())
        {
            name = reader.GetString(1);
            desc = reader.GetString(2);
            date = reader.GetString(3);
            exp = reader.GetInt16(4);
            gold = reader.GetInt16(5);
            diff = reader.GetInt16(6);
        }
        reader.Dispose();
        Debug.Log("Evenimentul " + name + " a fost adaugat.");

        if (exp != 0)
        {
            DateTime aux_date = DateTime.UtcNow;
            aux_date = aux_date.AddDays(7);
            Debug.Log("Data curenta " + DateTime.UtcNow);
            Debug.Log("Data curenta " + aux_date);
            sql = $"INSERT INTO todo(player_id, type, name, desc, date, exp, gold, diff) " +
                $"VALUES ({pl.id}, 6, '{name}', '{desc}', '{aux_date.ToString("d.M.yyyy")}', {exp}, {gold}, {diff})";
            dbcmd.CommandText = sql;
            reader = dbcmd.ExecuteReader();
            reader.Dispose();
        }
        dbcmd.Dispose();
        dbcon.Close();
    }

    // Update is called once per frame
    void Gameover()
    {
        //sterge tot din baza de date de la playerul respectiv
        SetDb();
        string sql = $"DELETE FROM todo WHERE player_id={pl.id}";
        dbcmd.CommandText = sql;
        IDataReader reader = dbcmd.ExecuteReader();
        reader.Dispose();
        //am sters taskurile
        sql = $"DELETE FROM equip WHERE id_player={pl.id}";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        reader.Dispose();
        //am sters equip
        sql = $"DELETE FROM inventory WHERE id_player={pl.id}";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        reader.Dispose();
        //am sters inventory
        sql = $"DELETE FROM effects_inventory WHERE id_player={pl.id}";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        reader.Dispose();
        //am sters effects_inventory
        sql = $"DELETE FROM player WHERE id={pl.id}";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        reader.Dispose();
        //am sters playerul
        dbcmd.Dispose();
        dbcon.Close();
        SceneManager.LoadScene(0);
    }
}