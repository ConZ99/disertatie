using Mono.Data.Sqlite;
using System;
using System.Data;
using TMPro;
using UnityEngine;
using System.Globalization;

public class Buttons : MonoBehaviour
{
    public GameObject parent;
    Player player;
    AddTask task;

    string conn;
    IDbConnection dbcon;
    IDbCommand dbcmd;
    IDataReader reader;
    void SetDb()
    {
        conn = "URI=file:" + Application.dataPath + "/db.db";
        dbcon = (IDbConnection)new SqliteConnection(conn);
        dbcon.Open();
        dbcmd = dbcon.CreateCommand();
    }

    private void Start()
    {
        SetDb();
        player = FindObjectOfType<Player>();
        task = FindObjectOfType<AddTask>();
    }

    public void Plus()
    {
        SetDb();
        //primeste rewards
        var list = parent.GetComponentsInChildren<TMP_Text>();
        string name = list[0].text;
        int id = Int16.Parse(list[4].text);
        int exp = 0, gold = 0;
        string sql = $"SELECT * FROM todo WHERE id='{id}'";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        while (reader.Read())
        {
            exp = reader.GetInt16(6);
            gold = reader.GetInt16(7);
        }
        reader.Dispose();
        dbcmd.Dispose();
        dbcon.Close();

        player.pl.previousAchievementValue = player.pl.achievementTracker[0];
        player.pl.achievementTracker[0] += gold;
        player.incrementAchievement(0);
        player.pl.previousAchievementValue = player.pl.achievementTracker[1];
        player.pl.achievementTracker[1]++;
        player.incrementAchievement(1);

        if (player.pl.clas == 1)
            player.AddRewards(exp + 5, gold, 0);
        if (player.pl.clas == 2)
            player.AddRewards(exp - 5, gold, 0);
        if (player.pl.clas == 3)
            player.AddRewards(exp, gold, 0);
        task.ShowTasks();
    }

    public void Minus()
    {
        SetDb();
        //primeste rewards
        var list = parent.GetComponentsInChildren<TMP_Text>();
        string name = list[0].text;
        int hp_minus = 0;
        int id = Int16.Parse(list[4].text);
        string sql = $"SELECT * FROM todo WHERE id='{id}'";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        while (reader.Read())
        {
            hp_minus = reader.GetInt16(8);
        }
        reader.Dispose();
        dbcmd.Dispose();
        dbcon.Close();
        player.AddRewards(0, 0, hp_minus);
        task.ShowTasks();
    }

    public void Complete()
    {
        SetDb();
        //primeste rewards
        var list = parent.GetComponentsInChildren<TMP_Text>();
        string name = list[0].text;
        int exp = 0, gold = 0;
        int id = Int16.Parse(list[4].text);
        string sql = $"SELECT * FROM todo WHERE id='{id}'";
        int type = 0;
        int remainingOccurences = 0;
        int maxOccurences = 0;
        DateTime date = new DateTime();
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        while (reader.Read())
        {
            exp = reader.GetInt16(6);
            gold = reader.GetInt16(7);
            type = reader.GetInt16(2);
            remainingOccurences = reader.GetInt16(11);
            maxOccurences = reader.GetInt16(10);
            date = DateTime.ParseExact(reader.GetString(5), "d.M.yyyy", null);
        }
        reader.Dispose();

        remainingOccurences--;
        player.pl.previousAchievementValue = player.pl.achievementTracker[0];
        player.pl.achievementTracker[0] += gold;
        player.incrementAchievement(0);

        if (player.pl.clas == 1)
            player.AddRewards(exp + 5, gold, 0);
        if (player.pl.clas == 2)
            player.AddRewards(exp - 5, gold, 0);
        if (player.pl.clas == 3)
            player.AddRewards(exp, gold, 0);

        if(remainingOccurences > 0)
        {
            sql = $"UPDATE todo SET remainingOccurences='{remainingOccurences}' WHERE id='{id}'";

            dbcmd.CommandText = sql;
            reader = dbcmd.ExecuteReader();
            reader.Dispose();
            dbcmd.Dispose();
            dbcon.Close();

            task.ShowTasks();

            return;
        }

        Calendar cal = CultureInfo.InvariantCulture.Calendar;
        DateTime aux = DateTime.UtcNow;

        if (type == 2)
        {
            player.pl.previousAchievementValue = player.pl.achievementTracker[3];
            player.pl.achievementTracker[3]++;
            player.incrementAchievement(2);
            sql = $"UPDATE todo SET date='{cal.AddDays(date, 1).ToString("d.M.yyyy")}', remainingOccurences={maxOccurences} WHERE id='{id}'";
        }
        if (type == 3)
        {
            player.pl.previousAchievementValue = player.pl.achievementTracker[4];
            player.pl.achievementTracker[4]++;
            player.incrementAchievement(3);
            sql = $"UPDATE todo SET date='{cal.AddWeeks(date, 1).ToString("d.M.yyyy")}', remainingOccurences={maxOccurences} WHERE id='{id}'";
        }
        if (type == 4)
        {
            player.pl.previousAchievementValue = player.pl.achievementTracker[5];
            player.pl.achievementTracker[5]++;
            player.incrementAchievement(4);
            sql = $"UPDATE todo SET date='{cal.AddMonths(date, 1).ToString("d.M.yyyy")}', remainingOccurences={maxOccurences} WHERE id='{id}'";
        }
        if (type == 5)
        {
            player.pl.previousAchievementValue = player.pl.achievementTracker[2];
            player.pl.achievementTracker[2]++;
            player.incrementAchievement(5);
            sql = $"DELETE FROM todo WHERE id='{id}'";
        }

        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        reader.Dispose();
        dbcmd.Dispose();
        dbcon.Close();

        task.ShowTasks();
    }

    public void Edit()
    {
        var list = parent.GetComponentsInChildren<TMP_Text>();
        string name = list[0].text;
        int id = 0;
        int idinit = Int16.Parse(list[4].text);
        string sql = $"SELECT id FROM todo WHERE id='{idinit}'";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        while (reader.Read())
            id = reader.GetInt16(0);
        reader.Dispose();
        dbcmd.Dispose();
        dbcon.Close();
        task.EditButton(id);
    }

    public void Delete()
    {
        var list = parent.GetComponentsInChildren<TMP_Text>();
        string name = list[0].text;
        int id = Int16.Parse(list[4].text);
        string sql = $"DELETE FROM todo WHERE id='{id}'";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        reader.Dispose();
        dbcmd.Dispose();
        dbcon.Close();
        task.ShowTasks();
    }
}
