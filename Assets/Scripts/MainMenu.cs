using Mono.Data.Sqlite;
using System.Collections;
using System.Data;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public GameObject inputField;
    public GameObject classChoice;
    string conn;
    IDbConnection dbcon;
    IDbCommand dbcmd;
    IDataReader reader;

    public GameObject loginScreen;
    public GameObject registerScreen;
    public Button login;
    public Button register;

    void SetDb()
    {
        conn = "URI=file:" + Application.dataPath + "/db.db";
        dbcon = (IDbConnection)new SqliteConnection(conn);
        dbcon.Open();
        dbcmd = dbcon.CreateCommand();
    }

    public void SubmitTxt()
    {
        SetDb();
        Debug.Log("wasser fuck!");
        string text = inputField.GetComponent<TMP_InputField>().text;
        string sql = "SELECT name FROM player";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        Debug.Log("wasser fuck2!");
        while (reader.Read())
        {
            Debug.Log("asdqw " + reader.GetString(0));
            if (reader.GetString(0) == null)
            {
                Debug.Log("not ok");
            }
            else if (reader.GetString(0).Equals(text))
            {
                Debug.Log("ok");
                StartGame();
            }
            else if (!reader.GetString(0).Equals(text))
            {
                Debug.Log("add player");
                classChoice.SetActive(true);
            }
        }
        if (!reader.Read())
        {
            classChoice.SetActive(true);
        }
        Debug.Log("wasser fuck3!");
        reader.Dispose();
        dbcmd.Dispose();
        dbcon.Close();
    }

    public void CreateWizard()
    {
        SetDb();
        string text = inputField.GetComponent<TMP_InputField>().text;
        Debug.Log("wizard");
        string sql = $"INSERT INTO player(name, class, hp) VALUES('{text}', 1, 75)";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        reader.Dispose();
        dbcmd.Dispose();
        dbcon.Close();
        StartGame();
    }

    public void CreateFighter()
    {
        SetDb();
        string text = inputField.GetComponent<TMP_InputField>().text;
        Debug.Log("fighter");
        string sql = $"INSERT INTO player(name, class, hp) VALUES('{text}', 2, 125)";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        reader.Dispose();
        dbcmd.Dispose();
        dbcon.Close();
        StartGame();
    }

    public void CreateMonk()
    {
        SetDb();
        string text = inputField.GetComponent<TMP_InputField>().text;
        Debug.Log("monk");
        string sql = $"INSERT INTO player(name, class, hp) VALUES('{text}', 3, 100)"; ;
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        reader.Dispose();
        dbcmd.Dispose();
        dbcon.Close();
        StartGame();
    }

    ArrayList names;

    private void Start()
    {
        names = new ArrayList();
        GetPlayerNames();
    }

    public void GetPlayerNames()
    {
        SetDb();
        string sql = $"SELECT name FROM player";
        dbcmd.CommandText = sql;
        reader = dbcmd.ExecuteReader();
        while (reader.Read())
        {
            names.Add(reader.GetString(0));
        }
        reader.Dispose();
        dbcmd.Dispose();
        dbcon.Close();
    }


    public void StartGame()
    {
        PlayerPrefs.SetString("name", inputField.GetComponent<TMP_InputField>().text);
        Debug.Log(inputField.GetComponent<TMP_InputField>().text);
        SceneManager.LoadScene(1);
    }

    public void Login()
    {
        loginScreen.SetActive(true);
        registerScreen.SetActive(false);
        login.interactable = false;
        register.interactable = true;
        FillDropdown();
    }
    public TMP_Dropdown drop;
    public void FillDropdown()
    {
        drop.options.Clear();
        foreach (string c in names)
            drop.options.Add(new TMP_Dropdown.OptionData(c));
        drop.RefreshShownValue();
    }

    public void btn_start()
    {
        PlayerPrefs.SetString("name", drop.captionText.text);
        SceneManager.LoadScene(1);
    }
    public void Register()
    {
        loginScreen.SetActive(false);
        registerScreen.SetActive(true);
        login.interactable = true;
        register.interactable = false;
    }
}
