using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Achievements : MonoBehaviour
{
    public Player player;
    TMP_Text[] achievementsRequirements;
    public GameObject achievementsScreen;
    public GameObject menus;
    string[] colors = { "#96ccfa", "#3c3bf1", "#77f689" };
    List<TMP_Text> achList;

    public void Toggle()
    {
        achievementsScreen.SetActive(!achievementsScreen.activeSelf);
        menus.SetActive(!menus.activeSelf);
    }

    public void CheckAchievements()
    {
        achievementsRequirements = achievementsScreen.GetComponentsInChildren<TMP_Text>();
        achList = new List<TMP_Text>();
        for (int i = 0; i < achievementsRequirements.Length; i++)
        {
            achList.Add(achievementsRequirements[i]);
        }
        foreach (TMP_Text acc in achList)
        {
            Debug.Log(acc.text);
        }

        if (achList.Count != 16)
            return;

        TotalCompletedHobbies();
        TotalCompletedDailies();
        TotalCompletedWeekly();
        TotalCompletedMonthly();
        TotalCompletedTodos();
        TotalEarnedGold();
        TotalBoughtItems();
        TotalBoughtEffects();
    }

    public void TotalCompletedHobbies()
    {
        if (player.pl.achievementTracker[1] >= 30)
        {
            achList[0].text = "<color=#f8f51b>Hobby addict!</color>";
            achList[1].text = "<color=#1b92f8>Completed!</color>";
        }
        else if (player.pl.achievementTracker[1] < 30 && player.pl.achievementTracker[1] >= 20)
        {
            int index;
            if (player.pl.achievementTracker[1] <= 23)
                index = 0;
            else if (player.pl.achievementTracker[1] <= 27)
                index = 1;
            else index = 2;
            achList[0].text = "<color=#f8f51b>Hobby addict!</color>";
            achList[1].text = "<color=" + colors[index] + ">" + player.pl.achievementTracker[1] + "/30</color>\nHobbies Completed";
        }
        else if (player.pl.achievementTracker[1] < 20 && player.pl.achievementTracker[1] >= 10)
        {
            int index;
            if (player.pl.achievementTracker[1] <= 13)
                index = 0;
            else if (player.pl.achievementTracker[1] <= 17)
                index = 1;
            else index = 2;
            achList[0].text = "<color=#0bda51>Hobby adept!</color>";
            achList[1].text = "<color=" + colors[index] + ">" + player.pl.achievementTracker[1] + "/20\nHobbies Completed";
        }
        else if (player.pl.achievementTracker[1] < 10)
        {
            int index;
            if (player.pl.achievementTracker[1] <= 3)
                index = 0;
            else if (player.pl.achievementTracker[1] <= 7)
                index = 1;
            else index = 2;
            achList[0].text = "<color=#ccee7b>Hobby noob!</color>";
            achList[1].text = "<color=" + colors[index] + ">" + player.pl.achievementTracker[1] + "/10\nHobbies Completed";
        }
    }

    public void TotalCompletedDailies()
    {
        if (player.pl.achievementTracker[3] >= 30)
        {
            achList[2].text = "<color=#f8f51b>Daily addict!</color>";
            achList[3].text = "<color=#1b92f8>Completed!</color>";
        }
        else if (player.pl.achievementTracker[3] < 30 && player.pl.achievementTracker[3] >= 20)
        {
            int index;
            if (player.pl.achievementTracker[3] <= 23)
                index = 0;
            else if (player.pl.achievementTracker[3] <= 27)
                index = 1;
            else index = 2;
            achList[2].text = "<color=#f8f51b>Daily addict!</color>";
            achList[3].text = "<color=" + colors[index] + ">" + player.pl.achievementTracker[3] + "/30</color>\nDailies Completed";
        }
        else if (player.pl.achievementTracker[3] < 20 && player.pl.achievementTracker[3] >= 10)
        {
            int index;
            if (player.pl.achievementTracker[3] <= 13)
                index = 0;
            else if (player.pl.achievementTracker[3] <= 17)
                index = 1;
            else index = 2;
            achList[2].text = "<color=#0bda51>Daily adept!</color>";
            achList[3].text = "<color=" + colors[index] + ">" + player.pl.achievementTracker[3] + "/20\nDailies Completed";
        }
        else if (player.pl.achievementTracker[3] < 10)
        {
            int index;
            if (player.pl.achievementTracker[3] <= 3)
                index = 0;
            else if (player.pl.achievementTracker[3] <= 7)
                index = 1;
            else index = 2;
            achList[2].text = "<color=#ccee7b>Daily noob!</color>";
            achList[3].text = "<color=" + colors[index] + ">" + player.pl.achievementTracker[3] + "/10\nDailies Completed";
        }
    }

    public void TotalCompletedWeekly()
    {
        if (player.pl.achievementTracker[4] >= 20)
        {
            achList[4].text = "<color=#f8f51b>Weekly addict!</color>";
            achList[5].text = "<color=#1b92f8>Completed!</color>";
        }
        else if (player.pl.achievementTracker[4] < 20 && player.pl.achievementTracker[4] >= 10)
        {
            int index;
            if (player.pl.achievementTracker[4] <= 13)
                index = 0;
            else if (player.pl.achievementTracker[4] <= 17)
                index = 1;
            else index = 2;
            achList[4].text = "<color=#f8f51b>Weekly addict!</color>";
            achList[5].text = "<color=" + colors[index] + ">" + player.pl.achievementTracker[4] + "/20</color>\nWeeklies Completed";
        }
        else if (player.pl.achievementTracker[4] < 10 && player.pl.achievementTracker[4] >= 5)
        {
            int index;
            if (player.pl.achievementTracker[4] <= 6)
                index = 0;
            else if (player.pl.achievementTracker[4] <= 8)
                index = 1;
            else index = 2;
            achList[4].text = "<color=#0bda51>Weekly adept!</color>";
            achList[5].text = "<color=" + colors[index] + ">" + player.pl.achievementTracker[4] + "/10\nWeeklies Completed";
        }
        else if (player.pl.achievementTracker[4] < 5)
        {
            int index;
            if (player.pl.achievementTracker[4] <= 2)
                index = 0;
            else if (player.pl.achievementTracker[4] <= 4)
                index = 1;
            else index = 2;
            achList[4].text = "<color=#ccee7b>Weekly noob!</color>";
            achList[5].text = "<color=" + colors[index] + ">" + player.pl.achievementTracker[4] + "/5\nWeeklies Completed";
        }
    }

    public void TotalCompletedMonthly()
    {
        if (player.pl.achievementTracker[5] >= 9)
        {
            achList[6].text = "<color=#f8f51b>Monthly addict!</color>";
            achList[7].text = "<color=#1b92f8>Completed!</color>";
        }
        else if (player.pl.achievementTracker[5] < 9 && player.pl.achievementTracker[5] >= 6)
        {
            int index;
            if (player.pl.achievementTracker[5] <= 6)
                index = 0;
            else if (player.pl.achievementTracker[5] <= 7)
                index = 1;
            else index = 2;
            achList[6].text = "<color=#f8f51b>Monthly addict!</color>";
            achList[7].text = "<color=" + colors[index] + ">" + player.pl.achievementTracker[5] + "/9</color>\nMonthlies Completed";
        }
        else if (player.pl.achievementTracker[5] < 6 && player.pl.achievementTracker[5] >= 3)
        {
            int index;
            if (player.pl.achievementTracker[5] <= 3)
                index = 0;
            else if (player.pl.achievementTracker[5] <= 4)
                index = 1;
            else index = 2;
            achList[6].text = "<color=#0bda51>Monthly adept!</color>";
            achList[7].text = "<color=" + colors[index] + ">" + player.pl.achievementTracker[5] + "/6\nMonthlies Completed";
        }
        else if (player.pl.achievementTracker[5] < 3)
        {
            int index;
            if (player.pl.achievementTracker[5] <= 0)
                index = 0;
            else if (player.pl.achievementTracker[5] <= 1)
                index = 1;
            else index = 2;
            achList[6].text = "<color=#ccee7b>Monthly noob!</color>";
            achList[7].text = "<color=" + colors[index] + ">" + player.pl.achievementTracker[5] + "/3\nMonthlies Completed";
        }
    }

    public void TotalCompletedTodos()
    {
        if (player.pl.achievementTracker[2] >= 30)
        {
            achList[8].text = "<color=#f8f51b>Todos or not Todos!</color>";
            achList[9].text = "<color=#1b92f8>Completed!</color>";
        }
        else if (player.pl.achievementTracker[2] < 30 && player.pl.achievementTracker[2] >= 20)
        {
            int index;
            if (player.pl.achievementTracker[2] <= 23)
                index = 0;
            else if (player.pl.achievementTracker[2] <= 27)
                index = 1;
            else index = 2;
            achList[8].text = "<color=#f8f51b>Todos or not Todos!</color>";
            achList[9].text = "<color=" + colors[index] + ">" + player.pl.achievementTracker[2] + "/30</color>\nTodos Completed";
        }
        else if (player.pl.achievementTracker[2] < 20 && player.pl.achievementTracker[2] >= 10)
        {
            int index;
            if (player.pl.achievementTracker[2] <= 13)
                index = 0;
            else if (player.pl.achievementTracker[2] <= 17)
                index = 1;
            else index = 2;
            achList[8].text = "<color=#0bda51>Todos Manager!</color>";
            achList[9].text = "<color=" + colors[index] + ">" + player.pl.achievementTracker[2] + "/20\nTodos Completed";
        }
        else if (player.pl.achievementTracker[2] < 10)
        {
            int index;
            if (player.pl.achievementTracker[2] <= 3)
                index = 0;
            else if (player.pl.achievementTracker[2] <= 7)
                index = 1;
            else index = 2;
            achList[8].text = "<color=#ccee7b>Todos noob!</color>";
            achList[9].text = "<color=" + colors[index] + ">" + player.pl.achievementTracker[2] + "/10\nTodos Completed";
        }
    }

    public void TotalEarnedGold()
    {
        if (player.pl.achievementTracker[0] >= 1000)
        {
            achList[10].text = "<color=#f8f51b>Money Bags!!</color>";
            achList[11].text = "<color=#1b92f8>Completed!</color>";
        }
        else if (player.pl.achievementTracker[0] < 1000 && player.pl.achievementTracker[0] >= 200)
        {
            int index;
            if (player.pl.achievementTracker[0] <= 500)
                index = 0;
            else if (player.pl.achievementTracker[0] <= 700)
                index = 1;
            else index = 2;
            achList[10].text = "<color=#f8f51b>Money Bags!!</color>";
            achList[11].text = "<color=" + colors[index] + ">" + player.pl.achievementTracker[0] + "/1000</color>\nEarned Gold";
        }
        else if (player.pl.achievementTracker[0] < 200 && player.pl.achievementTracker[0] >= 100)
        {
            int index;
            if (player.pl.achievementTracker[0] <= 130)
                index = 0;
            else if (player.pl.achievementTracker[0] <= 170)
                index = 1;
            else index = 2;
            achList[10].text = "<color=#0bda51>Entrepreneur!</color>";
            achList[11].text = "<color=" + colors[index] + ">" + player.pl.achievementTracker[0] + "/200\nEarned Gold";
        }
        else if (player.pl.achievementTracker[0] < 100)
        {
            int index;
            if (player.pl.achievementTracker[0] <= 30)
                index = 0;
            else if (player.pl.achievementTracker[0] <= 70)
                index = 1;
            else index = 2;
            achList[10].text = "<color=#ccee7b>Empty Pockets..</color>";
            achList[11].text = "<color=" + colors[index] + ">" + player.pl.achievementTracker[0] + "/100\nEarned Gold";
        }
    }

    public void TotalBoughtItems()
    {
        if (player.pl.achievementTracker[6] >= 30)
        {
            achList[12].text = "<color=#f8f51b>Monopoly</color>";
            achList[13].text = "<color=#1b92f8>Completed!</color>";
        }
        else if (player.pl.achievementTracker[6] < 30 && player.pl.achievementTracker[6] >= 20)
        {
            int index;
            if (player.pl.achievementTracker[6] <= 23)
                index = 0;
            else if (player.pl.achievementTracker[6] <= 27)
                index = 1;
            else index = 2;
            achList[12].text = "<color=#f8f51b>Monopoly</color>";
            achList[13].text = "<color=" + colors[index] + ">" + player.pl.achievementTracker[6] + "/30</color>\nBought Items";
        }
        else if (player.pl.achievementTracker[6] < 20 && player.pl.achievementTracker[6] >= 10)
        {
            int index;
            if (player.pl.achievementTracker[6] <= 13)
                index = 0;
            else if (player.pl.achievementTracker[6] <= 17)
                index = 1;
            else index = 2;
            achList[12].text = "<color=#0bda51>Equipment Hoarder</color>";
            achList[13].text = "<color=" + colors[index] + ">" + player.pl.achievementTracker[6] + "/20\nBought Items";
        }
        else if (player.pl.achievementTracker[6] < 10)
        {
            int index;
            if (player.pl.achievementTracker[6] <= 3)
                index = 0;
            else if (player.pl.achievementTracker[6] <= 7)
                index = 1;
            else index = 2;
            achList[12].text = "<color=#ccee7b>Starting off</color>";
            achList[13].text = "<color=" + colors[index] + ">" + player.pl.achievementTracker[6] + "/10\nBought Items";
        }
    }

    public void TotalBoughtEffects()
    {
        if (player.pl.achievementTracker[7] >= 5)
        {
            achList[14].text = "<color=#f8f51b>Grandmaster</color>";
            achList[15].text = "<color=#1b92f8>Completed!</color>";
        }
        else if (player.pl.achievementTracker[7] < 5 && player.pl.achievementTracker[7] >= 3)
        {
            int index = 2;
            achList[14].text = "<color=#f8f51b>Grandmaster</color>";
            achList[15].text = "<color=" + colors[index] + ">" + player.pl.achievementTracker[7] + "/5</color>\nBought Effects";
        }
        else if (player.pl.achievementTracker[7] < 3 && player.pl.achievementTracker[7] >= 1)
        {
            int index = 1;
            achList[14].text = "<color=#0bda51>Magika user</color>";
            achList[15].text = "<color=" + colors[index] + ">" + player.pl.achievementTracker[7] + "/3\nBought Effects";
        }
        else if (player.pl.achievementTracker[7] < 1)
        {
            int index = 0;
            achList[14].text = "<color=#ccee7b>Discovering magic</color>";
            achList[15].text = "<color=" + colors[index] + ">" + player.pl.achievementTracker[7] + "/1\nBought Effects";
        }
    }

}