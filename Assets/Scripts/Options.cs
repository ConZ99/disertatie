using TMPro;
using UnityEngine;

public class Options : MonoBehaviour
{
    public Camera cam;
    public TMP_Dropdown mydropdown;
    public GameObject game;
    public GameObject options;
    // Update is called once per frame  
    void Update()
    {
        switch (mydropdown.value)
        {
            case 0:
                cam.backgroundColor = Color.red;
                break;
            case 1:
                cam.backgroundColor = Color.blue;
                break;
            case 2:
                cam.backgroundColor = Color.green;
                break;
        }
    }


    public void Done()
    {
        game.SetActive(!game.active);
        options.SetActive(!options.active);
    }

    public GameObject shops;
    public GameObject inventories;
    public GameObject tasks;
    public GameObject menus;

    public void viewShops()
    {
        shops.SetActive(true);
        inventories.SetActive(false);
        tasks.SetActive(false);
        menus.SetActive(false);
    }
    public void viewInventories()
    {
        shops.SetActive(false);
        inventories.SetActive(true);
        tasks.SetActive(false);
        menus.SetActive(false);
    }
    public void viewTasks()
    {
        shops.SetActive(false);
        inventories.SetActive(false);
        tasks.SetActive(true);
        menus.SetActive(false);
    }

    public void back()
    {
        shops.SetActive(false);
        inventories.SetActive(false);
        tasks.SetActive(false);
        menus.SetActive(true);
    }
}
